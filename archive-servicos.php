<?php get_header() ?>
	
	<div class="servicos">
		
		<!--PART SERVICOS-->
			<div class="container content-servico">
				<div class="row">
					<div class="col-md-12">
						<h1 class="title-serv title-page"><?php echo get_option('servico_servico') ?></h1>
						<p class="page-subtitle sub-title-serv"></p>
					</div>
				</div>
			</div>
		
		
			<div class="container-fluid content-3">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="  col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-2 col-md-offset-1 col-sm-offset-2 container-oferecemos container-list-servicos ">
								
								<ul>
							<?php 
								$qry = new WP_Query(array('post_type' => 'servicos','posts_per_page' => 100,
									'tax_query' => array(
												array(
													'taxonomy' => 'categorias-servicos',
													'field'    => 'slug',
													'terms'    =>  'servicos',
												),)
								));
								
								
								while($qry->have_posts()){
									$qry->the_post();
							?>
									
									<li>
									  <figure class="slide">
									  	<a href="<?php the_permalink() ?>" title="<?php echo get_the_title() ?>">
									  		<?php the_post_thumbnail('thumb_block_sias', array( 'alt' =>  get_the_title(), 'title' => get_the_title() )) ?>
									  	</a>
									  	<figcaption><h3><?php the_title() ?></h3></figcaption>
									  </figure>
									</li>  
									
							<?php } ?>
									<!--<li>
										<figure class="slide">
											<a href="http://limpezaporhora.com.br/">
												<img src="http://limpeza.uniusdesenvolvimento.com.br/wp-content/uploads/2016/04/limpeza-por-hora.png" style="width: 145px;"/>
											</a>
											<figcaption><h3>Limpeza Por Hora</h3></figcaption>
										</figure>
									</li>-->
								</ul>
								
							</div>
						</div>	
					</div>
				</div>
			</div>

		<!--CONTENT 6 HOME/UNIDADES -->
			<?php //get_template_part('partials/part-unidades') ?>
		
		<!--PART ORÇAMENTO-->
			<?php get_template_part('partials/part-form_orcamento') ?>

		<!--PART BLOG-->
			<?php get_template_part('partials/part-blog') ?>
	</div>-->
	
<?php get_footer() ?>
