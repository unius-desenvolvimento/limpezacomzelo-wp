<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package unius
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'artigos-1' );  ?>
	
</div><!-- #secondary -->
