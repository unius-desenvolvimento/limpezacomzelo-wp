<?php get_header(); ?>
	
	<div class="container container-single">
	
		<?php while(have_posts()){the_post(); ?>
			
				<div class="row">
					<div class="col-md-12 title-single"> 
						<h1 class="title-page"><?php the_title() ?></h1> 
						<p class="page-subtitle sub-title-serv"></p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-8  col-sm-8 col-xs-12 thumbnail-single">
				
						<figure>
							<a href="<?php the_permalink() ?>" title="<?php get_the_title() ?>">
								<?php the_post_thumbnail('thumbnail-single', array( 'alt' =>  get_the_title(), 'title' => get_the_title() )) ?>
							</a>
						</figure>
						
						
						<article>
							<header>
								<h2  class="article-header"> <?php the_title() ?></h2>
							</header>
							
							<div class="conteudo-single">
								<?php the_content() ?>
							</div>
							
							 <footer>
							 	<!-- <span class="author"> <strong><?php echo get_option('publicado_single') ?></strong> <?php  echo get_the_author()  ?></span> -->
							 	
							 	
							 	
							 	<ul class="social">
							 		<li><span  class='compartihar'><?php echo get_option('compartilhar_single') ?></span></li>
							 		
							 		<li> 
							 			<div class="fb-share-button"  data-href="<?php the_permalink() ?>"  data-layout="button"></div>
							 		</li>
							 		
							 		<li>
							 			
							 			<div class="g-plus" data-action="share" data-annotation="none" data-href="<?php the_permalink() ?>"></div>
							 			
							 		</li>
							 		
							 		<li>
							 			<script type="IN/Share" data-url="<?php the_permalink() ?>" data-counter="right"></script>
							 		</li>
							 		
							 	</ul>
							 </footer>
							
							
						</article>
					</div>
					
					<div class="col-md-3 col-sm-3 col-xs-12 sidebar"><?php  get_sidebar() ?></div>
				</div>
				
		<?php } ?>	
	</div>
	
	
	<div class="container-post-relacionados">
		<!--PART POST RELACIONADOS-->
			<?php get_template_part('partials/part-post_relacionados'); ?>		
	</div>
	
	<div class="container-fluid container-comments">
		<div class="row">
			<div class="container">
				<div class="row">
					<?php comments_template();?>				
				</div>
			</div>
		</div>
	</div>
	
		

<?php get_footer(); ?>
