// JavaScript Document

$(function(){

		$('.input-curriculo').change(function(){
			var files = $('.input-curriculo')[0].files[0];
			$('.input-false').empty(files['name']);
			$('.input-false').append(files['name']);
		});


	    /*ACTIVE DO MENU*/
		    $(".menu a").each(function() {
		        if(window.location.href == (this.href)) {
		            $(this).closest("li").addClass("active");
		        }
		    });

		/*Regras gerais*/
			$('.content-5 .bx-controls-direction a').empty();
			$('.estados-page-franquia:first-child, .cat-estado-page-franquia:first-child').addClass('active');

		/*JS Menu*/
			$('.menu').slicknav();

			$( "#menu-item-693" ).click(function() {
				/*$('html,body').animate({
					scrollTop: $(window.location.hash).offset().top-130
				});*/

                                $('html,body').animate({ scrollTop: 0 }, 'slow');
			});

			/*$("#menu-item-693").click(function(event){

        		event.preventDefault ? event.preventDefault() : (event.returnValue = false);

        		$('html,body').animate({ scrollTop: top-130 }, 'slow');

        			return false;

    			});*/
		//Execução depois de carregar o  DOM
			$(window).load(function(){

                // $('html,body').animate({ scrollTop: 0 }, 'slow');

				/*$('html,body').animate({ scrollTop: top-130 }, 'slow');*/
				/*section Absolute*/
					$('.container-faixa-azul').height($('.sub-container-index').height() - ((12 * $('.sub-container-index').height()) /100));
					$('.sub-container-index').css('margin-top', $('.image-fundo img').height() - $('#header').height() - (8 * $('.image-fundo img').height())/100  +'px');


					$(window).resize(function(){
				/*section Absolute*/
						$('.container-faixa-azul').height($('.sub-container-index').height() - ((12 * $('.sub-container-index').height()) /100));
						$('.sub-container-index').css('margin-top', $('.image-fundo img').height() - $('#header').height() - (8 * $('.image-fundo img').height())/100  +'px');
					});

			});

		/*Banner*/


			/*Banner Serviços Home*/
				$('.slider1').bxSlider({
				    slideWidth: 190,
				    minSlides: 2,
				    maxSlides: 5,
				    slideMargin: 10,
				    auto:false
				  });

			/*Banner Na Mídia*/
			var _width;
			var _numSliders;
			var _tamanhoTela = $(window).width();

			if(_tamanhoTela > 1000){
				_numSliders = 4;
				_width = _tamanhoTela / _numSliders; //Pega o tamanho da tela e ajusta para 4 elementos
				_width -= 16; //Retira 16 pois é 8 de margin para cada lado
			}else if(_tamanhoTela >= 768) {
				_numSliders = 3;
				_width = _tamanhoTela / _numSliders; //Pega o tamanho da tela e ajusta para 4 elementos
				_width -= 32;
			}else {
				_numSliders = 1;
			}



				$('.slider2').bxSlider({
				    slideWidth: _width,
				    minSlides: 1,
				    maxSlides: _numSliders,
				    slideMargin: 8,
				    auto:true
			  	});

			//Banner Depoimentos
				$('.bxslider2').bxSlider({
				  minSlides: 1,
				  maxSlides: 1,
				  slideWidth: 800,
				  slideMargin: 10,
				  auto:true
				});


		/*JS TABS VERTICAL FRANQUIAS*/
			$('.container-content-tab-intern .tab-pane:first-child, .ul-unidades li:first-child').addClass('active');


		/*AUTO COMPLETE DOS FORM E JMASK*/
			$('input[name="cep"]').blur(function(){

			var cep = $('input[name="cep"]').val();

			var url = "https://viacep.com.br/ws/"+cep+"/json/";

			$.getJSON(url, function(dadosRetorno){
					$('input[name="endereco"]').val(dadosRetorno.logradouro);
			 		$('input[name="cidade"]').val(dadosRetorno.localidade);
			 		$('input[name="bairro"]').val(dadosRetorno.bairro);
			 		$('input[name="estado"]').val(dadosRetorno.uf);
		 		});
			});

			$('input[name="cep"]').mask('99999-999');
			$('input[name="telefone"]').mask('(99) 9999-9999?9');
			$('input[name="celular"]').mask("(99) 9999-9999?9");

		//ROLAGEM SUAVE QUE LINKA PARA A DIV
			$("#top_header").click(function(){
			$('html,body').animate({ scrollTop: 0 }, 'slow');

		        return false;
		   });

		$(window).scroll(function () {

			var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();

			if (scrollBottom < 50) {
				$("#top_header").css('opacity','1');
			}else{
				$("#top_header").css('opacity','0');
			}

		});


	adaptHeight(".unidades-limpezaporhora-title .unidades-list");
	adjustBlocoTexto('.servicos-home');
		$(window).resize(function(){
		adaptHeight(".unidades-limpezaporhora-title .unidades-list");
		adjustBlocoTexto('.servicos-home');
	});





});

//Ajusta block da home "O Que podemos fazer
function adjustBlocoTexto(parent){
	if($(window).width() < 650){
		var height = $(parent).find('.text').outerHeight(true);
		height += 140;
		$(parent).find('.no-paddin').height(height);
		$(parent).find('.img.last').height((height));
	}

}


function showConfirmLimpezaComZelo(){
	$("#limpezacomzelo-form").fadeOut();
	$(".sent-ok-limpezacomzelo").fadeIn();
}
function showConfirmLimpezaPorHora(){
	$("#limpezaporhora-form").fadeOut();
	$(".sent-ok-limpezaporhora").fadeIn();

}

function adaptHeight(selector){

	if($(window).width() >= 768){
		var _maxHeight = -1;

		var array = $(selector);
		for(var i = 0; i < $(array).length;  i++){
			var _item = $(array)[i];
			if(_maxHeight < $(_item).height()){
				_maxHeight = $(_item).height();
			}
		}

		for(var i = 0; i < $(array).length;  i++){
			var _item = $(array)[i];

			$(_item).height(_maxHeight);

		}
	}

}




