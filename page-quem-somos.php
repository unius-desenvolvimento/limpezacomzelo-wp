<?php get_header() ?>


	<!--PART BEM VINDO-->
		<div class="part-bem-vindo-quem-somos">
			<?php get_template_part('partials/part-bem_vindo') ?>				
		</div>
	
	<!--PART TEXTO QUEM SOMOS-->
		<div class="container-fluid content-1-quem-somos">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="container">
							<div class="col-md-12">
								<?php while(have_posts()){ the_post() ?>
										<article>
											<h1 class="page-title-2-home page-title3 title-qum-somos"><?php the_title(); ?></h1>
											<p class="page-subtitle subtitle-quem-somos"></p>
											<?php the_content(); ?>
										</article>
								<?php } ?>	
							</div>
						</div>	
					</div>	
				</div>	
			</div>
		</div>
	
	<!--PART VANTAGENS-->
		<div class="part-vantagens-quem-somos">
			<?php get_template_part('partials/part-valores_quem_somos') ?>	
		</div>
	
	<!--PART DEPOIMENTOS-->
		<div class="part-depoimentos-quem-somos">
			<?php get_template_part('partials/part-depoimentos') ?>
		</div>
	
	<!--PART ORÇAMENTO-->
		<!--div class="part-orcamento-quem-somos">
			<//?php get_template_part('partials/part-form_orcamento') ?>
		</div-->
	
	<!--PART BLOG-->
		<div class="part-blog-quem-somos">
			<?php get_template_part('partials/part-blog') ?>
		</div>
	
<?php get_footer() ?>