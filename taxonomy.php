<?php
/**
 * The template for displaying taxonomy pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package unius
 */

get_header(); ?>
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <div id="content" class="site-content">
        <?php
          global $wp_query;
          $term = $wp_query->get_queried_object();
          $title = $term->name;
          $description = $term->description;
        ?>
        <?php
          while ( have_posts() ) {
            the_post();
        ?>
          <h1><?php echo get_the_title(); ?></h1>
        <?php
          }
          wp_reset_postdata();
        ?>
      </div>
    </main><!-- #main -->
  </div><!-- #primary -->
<?php get_footer(); ?>
