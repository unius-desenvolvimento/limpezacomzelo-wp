<?php get_header() ?>

<?php
		$catg = null;

		/*ARGUMENTOS PARA RETORNAR AS CATEGORIAS(ESTADOS) DAS UNIDADES*/
		$args = array(
		 'type'                     => 'seja-um-franqueado',
		 'child_of'                 => 0,
		 'parent'                   => 0,
		 'orderby'                  => '',
		 'order'                    => 'Asc',
		 'hide_empty'               => 0,
		 'hierarchical'             => 1,
		 'exclude'                  => '12',
		 'include'                  => '',
		 'number'                   => '',
		 'taxonomy'                 => 'categorias-unidades',
		 'pad_counts'               => false

		);

	/*PEGANDO AS CATEGORIAS FILTRADAS DE ACORDO COM OS ARGUMENTOS ACIMA*/
		$categories = get_categories($args);
		
		$arr_category = array();
		$duplicate_values = array();
			foreach($categories as $category) 
			{
				$index = get_field('order_estado', $category->taxonomy . '_' . $category->term_id);
				if($index >= 0 ){
					$arr_category[$index] = $category;
				}
				
			}


			ksort($arr_category);
			$categories = $arr_category;






?>
	
		<div class="container-fluid content-1-franquias">
			<div class="row">
				<div class="container">
					<div class="row">
						
						<?php 
							$qry = new WP_Query( array(
						  					"post_type" 	=> 'unidades', 
						  					'public'		=> true,
						  					'tax_query' => array(
												array(
													'taxonomy' => 'categorias-unidades',
													'field'    => 'slug',
													'terms'    =>  "cms-conteudo"
												),)));
						?>
						
							<?php while($qry->have_posts()){ $qry->the_post() ?>
									
									    
												<div class="col-md-12">
													<h2 class="title-serv title-page title-bem-vindo"><?php the_title() ?></h2>
												</div>
												
												<div class="col-md-12 conteudo-bem-vindo">
													<?php the_content() ?>
												</div>
									
									
							<?php } ?>
					</div>
				</div>
			</div>
		</div>
	
		<div class="container-fluid content-1-quem-somos content-header-tabs-franquias" style="min-height: 940px; padding-bottom: 65px;">
			<div class="row">
				<div class="container">
					<div class="row">
						
						<div class="col-md-12">
							<h1 class="title-serv title-page title-franquias"><?php echo get_option('franquias_unidades') ?></h1>
							<p class="page-subtitle sub-title-serv sub-title-franquias"></p>
						</div>
							
						<div class="col-md-12 row">
													
							<div class="col-md-6 col-sm-6 col-xs-12 container-unidades-left">
							<?php 
								$x = 1; 
								$rand = 60; 
								
								$middle = ceil( count($categories)/2 );

								
								foreach($categories as $category){ ?>
								  <?php if($x <= $middle ) { ?>
									<?php $cat1[] = $category->cat_ID;  ?>
									<?php
													$query = new WP_Query( array(
															  					"post_type" 	=> 'unidades', 'showposts' => 100,
															  					"orderby"			=> 'name',
															  					'public'			=> true,
															  					"order" => 'ASC',
															  					'tax_query' => array(
																					array(
																						'taxonomy' => 'categorias-unidades',
																						'field'    => 'slug',
																						'terms'    =>  $category->slug,
																					),)));
												?>
									<div class="container-cat">
										<h4 class="cat_name" style="display:<?php echo $query->have_posts() ? 'block' : 'none'; ?>">
											<?php 
												echo  $category->name;
											?>
										</h4>
										
											
											  <ul>
											    <div class="container-li">


												<?php 	while($query->have_posts()) { $query->the_post(); ?>

															<li>
																<a href="<?php echo   get_protocol(); the_field('linkunidade'); ?>" title="<?php echo get_the_title()  ?>">
																	<?php the_title() ?>
																</a>
															</li>
												<?php  }  }?>
												</div>
											</ul>
											
										
										<?php $x++; } ?>
									</div>

				
						</div>

						<?php ?>

<!---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->


							<?php

								/*ARGUMENTOS PARA RETORNAR AS CATEGORIAS(ESTADOS) DAS UNIDADES*/
									$cat1[] = 19;

									$args2 = array(
										 'type'                     => 'seja-um-franqueado',
										 'child_of'                 => 0,
										 'parent'                   => 0,
										 'orderby'                  => '',
										 'order'                    => 'Asc',
										 'hide_empty'               => 0,
										 'hierarchical'             => 1,
										 'exclude'                  => implode(',', $cat1),
										 'include'                  => '',
										 'number'                   => '',
										 'taxonomy'                 => 'categorias-unidades',
										 'pad_counts'               => false

										);

								/*PEGANDO AS CATEGORIAS FILTRADAS DE ACORDO COM OS ARGUMENTOS ACIMA*/

									// $categories2 = get_categories($args2);
								$last_index = $x - 1;
								
								
								
								
								$categories2 = array_slice($categories,$middle);
	                
   
	                
	              						
  
							?>

							<div class="col-md-6 col-sm-6 col-xs-12 container-unidades-right">

								<?php foreach($categories2 as $category){ $rand = 60; ?>

													<div class="container-cat">

														<ul>
															<?php
																$query = new WP_Query( array(
																		  					"post_type" 	=> 'unidades', 'showposts' => 100,
																		  					"orderby"			=> 'name',
																		  					"order" => 'ASC',
																		  					'tax_query' => array(
																								array(
																									'taxonomy' => 'categorias-unidades',
																									'field'    => 'slug',
																									'terms'    =>  $category->slug,
																								),)));
															?>
															<h4 class="cat_name" style="text-align: right;display:<?php echo $query->have_posts() ? 'block' : 'none'; ?>">
																 <?php echo $category->name; ?>
															</h4>

                              
															
															 <ul>
															  <div class="container-li">
																

																<?php 	while($query->have_posts()) { $query->the_post(); ?>

																			<li>
																				<a href="<?php echo   get_protocol(); the_field('linkunidade'); ?>" title="<?php echo get_the_title()  ?>" >
																					<?php the_title() ?>
																				</a>
																			</li>
																<?php  } ?>
																</div>
															</ul>
															
														
													</div>

								<?php } ?>

							</div>


							
							
						</div>
						
						
					</div>
				</div>
			</div>
		</div>	
		
	


	
		<div class="part-vantagens-quem-somos">
			<div class="container">
				<div class="sub-container-index">
					<div class="row">
						
						<div class="col-md-12">
							<div class="block-home">
								<h2 class="page-title3 title-franqueado"><?php echo get_option('seja_unidade') ?></h2>
								<p class="page-subtitle sub-title-franquias"></p>
							</div>
						</div>
						
						<div class="col-md-12 conteudo-seja-franqueado">
							<?php  echo get_option('seja_content_unidade')?>
						</div>
						
						<div class="col-md-12 seja-franqueado-img">
							<img src="http://limpezacomzelo.com.br/wp-content/uploads/2015/06/seja-um-franqueado.gif" alt="Limpeza com Zelo" title="Limpeza com Zelo" />
						</div>
					</div>
				</div>
			</div>
		</div>
	
	<!--PART DEPOIMENTOS-->
		<div class="part-depoimentos-quem-somos">
			<?php get_template_part('partials/part-depoimentos') ?>
		</div>

	<!--PART BLOG-->
		<div class="part-blog-quem-somos">
			<?php get_template_part('partials/part-blog') ?>
		</div>
	
<?php get_footer() ?>