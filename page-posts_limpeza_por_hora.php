<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 28/10/16
 * Time: 09:28
 *
 * Description: Page created to get the posts and to send to www.limpezaporhora.limpezacomzelo.com.br in the home of the website
 *
 */

$query = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 3));

$arr = array();

foreach ($query->posts as $post){

    $arr[] = array(
        'title'   => $post->post_title,
        'content' => $post->post_content,
        'excerpt' => $post->post_excerpt,
        'url'     => $post->post_name,
        'thumb'   => get_the_post_thumbnail_url($post->ID)
    );
}

header('Content-Type: application/json');
echo json_encode($arr);
return;
