<?php get_header() ?>
	
	<div class="single-servicos">
			<?php while(have_posts()){the_post(); ?>
					
					<div class="container conteudo">
						<div class="row">
							<div class="col-md-12">
								<article>
									<h1 class="page-title-2-home page-title3"><?php the_title(); ?></h1>
									<p class="page-subtitle sub-title-serv sub-title-franquias"></p>
									<?php the_content(); ?>
								</article>
							</div>
						</div>
					</div>
					
					<div class="container-fluid thumbnail-2">
						<div class="row">
							<?php get_the_post_thumbnail_secundary(); ?>
						</div>	
					</div>
				
			<?php } ?>

		<!--PART ORÇAMENTO-->
			<?php get_template_part('partials/part-form_orcamento') ?>
			
		<!--PART DEPOIMENTOS-->
			<div class="servicos">
				<?php get_template_part('partials/part-depoimentos') ?>
			</div>
			
		<!--PART BLOG-->
			<?php get_template_part('partials/part-blog') ?>
	</div>
	
<?php get_footer() ?>	

