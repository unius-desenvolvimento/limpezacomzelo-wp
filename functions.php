<?php
/**
 * unius functions and definitions
 *
 * @package unius
 */
/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'unius_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function unius_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on unius, use a find and replace
	 * to change 'unius' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'unius', get_template_directory() . '/languages' );
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'unius' ),
		'secondary' => __( 'Footer_menu', 'unius' ),
	) );
	
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'unius_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // unius_setup
add_action( 'after_setup_theme', 'unius_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
 
 // Necessary to force custom post types nice urls to work
function my_rewrite_flush() {
    flush_rewrite_rules(false);
}
add_action( 'after_switch_theme', 'my_rewrite_flush' );
 
function unius_widgets_init() {
	
	
	register_sidebar( array(
		
		'name'          => __( 'Sidebar', 'unius' ),
		
		'id'            => 'sidebar-1',
		
		'description'   => '',
		
		'before_title'  => '<h2 class="widget-title">',
		
		'after_title'   => '</h2>',
		
		'before_widget' => '<aside id="%1$s" class="widget %2$s ">',
		
		'after_widget'  => '</aside>',
		
		
		
	) );
	
	
	
}
add_action( 'widgets_init', 'unius_widgets_init' );

/**
 * Enqueue scripts and styles.
 */


function unius_scripts() {
	wp_enqueue_style( 'unius-style', get_stylesheet_uri() );
	
	
	// wp_enqueue_script( 'unius-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'unius_scripts' );



/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';
/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';
/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
* Thumbnail custom sizes
*/
if ( function_exists( 'add_image_size' ) ) {
		add_image_size( 'thumb', 262, 262, true );
		add_image_size( 'thumb_block_sias', 89, 89, true );
}
the_post_thumbnail('thumb');
the_post_thumbnail('thumb_block_sias');

/**
* Function to get a post slug by ID
*/
function the_slug($id) {
		$post_data = get_post($id, ARRAY_A);
		$slug = $post_data['post_name'];
		return $slug;
}
/**
*Custom Post Type Exemplo"
*/
// add_action( 'init', 'create_post_type_exemplo' );
// function create_post_type_exemplo() {
// 		register_post_type( 'exemplo',
// 			array(
// 			'labels' => array(
// 				'name' => __( 'Post Type Exemplos' ),
// 				'singular_name' => __( 'post' ),
// 				'all_items' => __('Todas os post'),
// 				'add_new' => __('Novo post'),
// 				'add_new_item' => __('Adicionar evento'),
// 			),
// 						'taxonomies'         => array('categoria-exemplo'), /*GET TAXONOMY - COMENTAR ESSA LINHA SE NÃO UTILIZAR TAXONOMIA*/
// 						'public'             => true,
// 						'publicly_queryable' => true,
// 						'show_ui'            => true,
// 						'show_in_menu'       => true,
// 						'query_var'          => true,
// 						'rewrite'            => array( 'slug' => 'exemplo' ),
// 						'capability_type'    => 'post',
// 						'has_archive'        => true,
// 						'hierarchical'       => true,
// 						'menu_position'      => 5,
// 						'menu_icon'   => 'dashicons-hammer',
// 						/* TROCAR ÍCONE https://developer.wordpress.org/resource/dashicons/ */
// 						'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
// 						/* array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields', ) */
// 				)
// 		);
// }

/**
*Taxonomy Exemplo COMENTAR ESSE BLOCO SE NÃO UTILIZAR TAXONOMIA
*/
// function create_exemplos_taxonomies() {
// 		$labels = array(
// 				'name' => _x( 'Categorias de Exemplos', 'taxonomy general name' ),
// 				'singular_name' => _x( 'Categoria de Exemplo', 'taxonomy singular name' ),
// 				'search_items' =>  __( 'Buscar Categorias' ),
// 				'popular_items' => __( 'Categorias Populares' ),
// 				'all_items' => __( 'Todas as Categorias' ),
// 				'parent_item' => null,
// 				'parent_item_colon' => null,
// 				'edit_item' => __( 'Editar Categoria' ),
// 				'update_item' => __( 'Atualizar Categoria' ),
// 				'add_new_item' => __( 'Adicionar Nova Categoria' ),
// 				'new_item_name' => __( 'Nome da Nova Categoria' ),
// 				'separate_items_with_commas' => __( 'Separe as Categorias por Virgulas' ),
// 				'add_or_remove_items' => __( 'Adicionar ou Remover Categorias' ),
// 				'choose_from_most_used' => __( 'Escolha as Categorias Mais Utilizadas' ),
// 				'menu_name' => __( 'Categorias de Exemplos' ),
// 		);
// 		register_taxonomy('categoria-exemplo','galeria-de-exemplos',array(
// 				'labels' => $labels,
// 				'show_ui' => true,
// 				'update_count_callback' => '_update_post_term_count',
// 				'rewrite' => array('slug' => 'categorias-exemplos'),
// 				'hierarchical' => true,
// 		));
// }
// add_action( 'init', 'create_exemplos_taxonomies', 0 );


/**
*LOGO ADMIN
*/
function my_login_logo() { ?>
		<style type="text/css">
				.login h1 a {
						background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png);
						padding-bottom: 10px;
						background-size: 160px;
						width: 160px;
				}
				.login form {
					margin-top: 20px;
					margin-left: 0;
					padding: 26px 24px 46px;
					font-weight: 400;
					overflow: hidden;
					background:none;
					box-shadow: none;
				}
				.wp-core-ui .button-primary {
					background: #ffcf00;
					border-color: #ffcf00;
					box-shadow:none;
					color: #4e1b5f;
					text-decoration: none;
					border-radius: 0px;
				}
				.wp-core-ui .button-primary.focus, .wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover {
					background: #4e1b5f;
					border-color: #4e1b5f;
					-webkit-box-shadow: inset 0 1px 0 rgba(120,200,230,.6);
					box-shadow: inset 0 1px 0 rgba(120,200,230,.6);
					color: #ffcf00;
				}
				.login #backtoblog a:hover, .login #nav a:hover, .login h1 a:hover {
					color: #4e1b5f;
				}
		</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo_url() {
		return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
		return 'Desenvolvido por Unius Multimidia';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

/**
* Menu Bootstrap
*/
require_once('wp_bootstrap_navwalker.php');
register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'THEMENAME' ),
) );

function wp_pagination($pages = '', $range = 9)
{  
    global $wp_query, $wp_rewrite;  
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;  
    $pagination = array(  
        'base' => @add_query_arg('page','%#%'),  
        'format' => '',  
        'total' => $wp_query->max_num_pages,  
        'current' => $current,  
        'show_all' => true,  
        'type' => 'plain', 
        'after_page_number'  => ' • '
        
    );  
    if ( $wp_rewrite->using_permalinks() ) $pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );  
    if ( !empty($wp_query->query_vars['s']) ) $pagination['add_args'] = array( 's' => get_query_var( 's' ) );  
    echo '<div class="wp_pagination">'.paginate_links( $pagination ).'</div>';
}



include_once('inc/custom-post.php');
include_once('inc/support.php');
include_once('inc/taxonomy.php');
include_once('inc/page_field_admin.php');



