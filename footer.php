<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package unius
 */
?>

		</div><!-- #content role="contentinfo"-->

		<footer id="colophon" class="site-footer">
			<div class="inner_footer">
				<div class="container-fluid footer-top">
						<div class="row">
							<div class="container">
								<div class="row row_menu_footer">

									<?php
											$menu_name = 'secondary';

										    $x=0;if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {

												$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
												$menu_items = wp_get_nav_menu_items($menu->term_id);

												$x=1;foreach ( (array) $menu_items as $key => $menu_item )
												{
													echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 box-telefone menu_'.$x.'">';

														    $title = $menu_item->title;
														    $url = $menu_item->url;
														    echo  '<a href="' . $url . '">' . $title . '</a>';

													echo '</div>';
												$x++;}

										    } else {echo "Menu não encontrado";}





									?>
								</div>
							</div>
						</div>

						<div class="container">
							<div class="row">

								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 box-telefone">
									<!-- COLOCAR A IMAGEM AQUI-->
										<!--<a href='http://www.franquia.limpezacomzelo.com.br/seja-um-franqueado/' target="_blank">
											<img class='img-responsive' src='/wp-content/themes/unius/img/Rodape.jpg' alt='Seja Um Franqueado'/>
										</a>-->
										<label><?php echo get_option('sociais_footer') ?></label>
										<ul>

											<?php if(get_option('facebook_unidade') != '#'){ ?>
												<li>
													<a href="<?php echo get_option('facebook_unidade') ?>"  target="_blank"  title="Fabook Limpeza com Zelo <?php echo get_option('name_unidade') ?>">
														<i class="fa fa-facebook-square"></i>
													</a>
												</li>
											<?php } ?>

											<?php if(get_option('google_plus_unidade') != '#'){ ?>
												<li>
													<a href="<?php echo get_option('google_plus_unidade') ?>"  target="_blank" title="Google Plus Limpeza com Zelo <?php echo get_option('name_unidade') ?>">
														<i class="fa fa-google-plus-square"></i>
													</a>
												</li>
											<?php } ?>


											<?php if(get_option('youtube_unidade') != '#'){ ?>
												<li>
													<a href="<?php echo get_option('youtube_unidade') ?>"  target="_blank" title="YouTube Limpeza com Zelo <?php echo get_option('name_unidade') ?>">
														<i class="fa fa-youtube-square"></i>
													</a>
												</li>
											<?php } ?>

											<?php if(get_option('linkedin_unidade') != '#'){ ?>
												<li>
													<a href="<?php echo get_option('linkedin_unidade') ?>"  target="_blank" title="Linkedin Limpeza com Zelo <?php echo get_option('name_unidade') ?>">
														<i class="fa fa-linkedin-square"></i>
													</a>
												</li>
											<?php } ?>

										</ul>
									<!-- FIM -->
								</div>

									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 box-sociais">
										<!-- INICIO DO TELEFONE -->
										<label><?php echo get_option('tel_footer') ?></label>

										<?php if(get_option('unidade_tel_1') != '' && get_option('unidade_tel_1') != '#'){ ?>
												<p><?php echo get_option('unidade_tel_1') ?></p>
										<?php } ?>

										<?php if(get_option('unidade_tel_2') != '' && get_option('unidade_tel_2') != '#'){ ?>
												<p><?php echo get_option('unidade_tel_2') ?></p>
										<?php } ?>

										<?php if(get_option('unidade_tel_3') != '' && get_option('unidade_tel_3') != '#'){ ?>
												<p><?php echo get_option('unidade_tel_3') ?></p>
										<?php } ?>

										<?php if(get_option('unidade_tel_4') != '' && get_option('unidade_tel_4') != '#'){ ?>
												<p><?php echo get_option('unidade_tel_4') ?></p>
										<?php } ?>

										<?php if(get_option('unidade_tel_5') != '' && get_option('unidade_tel_5') != '#'){ ?>
												<p><?php echo get_option('unidade_tel_5') ?></p>
										<?php } ?> <!-- FINAL DO TELEFONE -->

										
									</div>

								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 box-endereco">
									<address>
										<?php echo get_option('unidade_endereco') ?>
									</address>
								</div>

							</div>
						</div>
					</div>
			</div>

				<div class="container-fluid footer-bottom">
					<div class="row">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<span><?php echo get_option('copyright_footer') ?><a href="https://www.unius.com.br/" target="_blank"><strong>UNIUS</strong></a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			<div class="btn-footer-to-top">
				<a href="#header" id="top_header">
					<i class="fa fa-chevron-circle-up"></i>
				</a>
			</div>

		</footer>

	<?php wp_footer(); ?>
	<!-- Google Tag Manager -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PMSMXF"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-PMSMXF');</script>

<!-- End Google Tag Manager -->
	</body>
</html>
