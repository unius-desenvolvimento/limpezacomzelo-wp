<?php get_header() ?>

<?php 
		$catg = null;
		
		/*ARGUMENTOS PARA RETORNAR AS CATEGORIAS(ESTADOS) DAS UNIDADES*/
		$args = array(
		 'type'                     => 'unidades',
		 'child_of'                 => 0,
		 'parent'                   => 0,
		 'orderby'                  => 'name',
		 'order'                    => 'ASC',
		 'hide_empty'               => 0,
		 'hierarchical'             => 1,
		 'exclude'                  => '12',
		 'include'                  => '',
		 'number'                   => '',
		 'taxonomy'                 => 'categorias-unidades',
		 'pad_counts'               => false 
	
		);
		
	/*PEGANDO AS CATEGORIAS FILTRADAS DE ACORDO COM OS ARGUMENTOS ACIMA*/
		$categories = get_categories($args);
		
		
?>
	
		<div class="container-fluid content-1-franquias">
			<div class="row">
				<div class="container">
					<div class="row">
						
						<?php 
							$qry = new WP_Query( array(
						  					"post_type" 	=> 'unidades', 
						  					'tax_query' => array(
												array(
													'taxonomy' => 'categorias-unidades',
													'field'    => 'slug',
													'terms'    =>  "cms-conteudo"
												),)));
						?>
						
							<?php while($qry->have_posts()){ $qry->the_post() ?>
									
									
												<div class="col-md-12">
													<h2 class="title-serv title-page title-bem-vindo"><?php the_title() ?></h2>
												</div>
												
												<div class="col-md-12 conteudo-bem-vindo">
													<?php the_content() ?>
												</div>
									
									
							<?php } ?>
					</div>
				</div>
			</div>
		</div>
	
		<div class="container-fluid content-1-quem-somos content-header-tabs-franquias">
			<div class="row">
				<div class="container">
					<div class="row">
						
						<div class="col-md-12">
							<h1 class="title-serv title-page title-franquias"><?php echo get_option('franquias_unidades') ?></h1>
							<p class="page-subtitle sub-title-serv sub-title-franquias"></p>
						</div>
							
						<div class="col-md-12">
						
							<div>
								<!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">
									
									<?php foreach($categories as $category){ $catg[] = $category->slug;  ?>
									    
										    <li role="presentation" class="cat-estado-page-franquia">
										    	<a href="#<?php echo $category->slug ?>" aria-controls="<?php echo $category->slug ?>" role="tab" data-toggle="tab" 
										    	title="<?php echo $category->name ?>"><h3><?php echo $category->slug ?></h3></a>
										    </li>
											    
								    <?php } ?>
								  </ul>
							</div>
							
						</div>
						
						
					</div>
				</div>
			</div>
		</div>	
		
	
		<div class="container-fluid content-1-quem-somos content-tabs-franquias">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="container">
							
							<div class="col-md-12">
								
								<div>
								  <!-- Tab panes -->
									<div class="tab-content">
									  	<?php $x=0; while($x != count($catg)){ ?>	
											  		
											  		<?php  
											  		
											  			/*Loop dos posts com o termo filtado da categoria*/
								  						  $query = new WP_Query( array(
													  					"post_type" 	=> 'unidades', 'posts_per_page' => 100,
													  					"orderby"		=> 'name',
													  					'order'			=> 'ASC',
													  					'tax_query' => array(
																			array(
																				'taxonomy' => 'categorias-unidades',
																				'field'    => 'slug',
																				'terms'    =>  $catg[$x],
																			),)));
													?>							
															<!-- Conteúdo da tas dos estados-->
																<div role="tabpanel" class="tab-pane estados-page-franquia" id="<?php echo $catg[$x] ?>">
																
																		
																			<div class="col-md-5 conteudo-links-tab-unidade">
																				<ul class="nav nav-tabs ul-unidades" role="tablist">
																					
																					<!--Loop da tab das unidades termo passado para identificar o link da tab é o slug -->	
																						<?php 	while($query->have_posts()) { $query->the_post(); ?>
																						
																									<li role="presentation" >
																										<a href="#<?php echo $post->post_name; ?>" 
																										aria-controls="<?php echo $post->post_name; ?>" role="tab" 
																										data-toggle="tab" title="<?php echo get_the_title()  ?>" ><h4><?php the_title() ?></h4></a>
																									</li>
																						<?php  } ?>
																				</ul>
																			</div>
																			
																			<div class="col-md-7 conteudo-tab-unidade">
																				<div class="tab-content container-content-tab-intern">
																					<!--loop do conteúdo da unidade termo passado para identificar a tab é o slug-->
																					<?php 	while($query->have_posts()) { $query->the_post(); ?>
																								
																								<div role="tabpanel" class="tab-pane" id="<?php echo $post->post_name; ?>">
																									<?php echo the_content(); ?>
																									
																									<?php if( get_field('linkunidade') ): ?>
																										
																										<div class="container-mais container-mais-left">
																											
																											<?php if( get_field('telefone') != ""){ ?>
																												<a href="tel:<?php echo the_field('telefone'); ?>" 
																													class="saiba-mais-unidade"
																													title="Saida mais sobre a unidade limpeza com zelo <?php echo get_the_title()  ?>" >
																													
																													<?php echo the_field('telefone'); ?>
																												</a>
																											<?php } ?>
																										</div>
																										
																										<div class="container-mais">
																											<a href="<?php echo   get_protocol(); the_field('linkunidade'); ?>" target="_self" 
																											class="saiba-mais-unidade" title="Saida mais sobre a unidade limpeza com zelo <?php echo get_the_title()  ?>" >SAIBA MAIS</a>
																										</div>
																									<?php endif; ?>
																								</div>
																								
																					<?php  } ?>
																				</div>		
																			</div>	
																</div>

												    
										<?php $x++; } ?>
									</div>
								</div>
								
							</div>
							
						</div>	
					</div>	
				</div>	
			</div>
		</div>

	
		<div class="part-vantagens-quem-somos">
			<div class="container">
				<div class="sub-container-index">
					<div class="row">
						
						<div class="col-md-12">
							<div class="block-home">
								<h2 class="page-title3 title-franqueado"><?php echo get_option('seja_unidade') ?></h2>
								<p class="page-subtitle sub-title-franquias"></p>
							</div>
						</div>
						
						<div class="col-md-12 conteudo-seja-franqueado">
							<?php  echo get_option('seja_content_unidade')?>
						</div>
						
						<div class="col-md-12 seja-franqueado-img">
							<img src="http://limpezacomzelo.com.br/wp-content/uploads/2015/06/seja-um-franqueado.gif" alt="Limpeza com Zelo" title="Limpeza com Zelo" />
						</div>
					</div>
				</div>
			</div>
		</div>
	
	<!--PART DEPOIMENTOS-->
		<div class="part-depoimentos-quem-somos">
			<?php get_template_part('partials/part-depoimentos') ?>
		</div>

	<!--PART BLOG-->
		<div class="part-blog-quem-somos">
			<?php get_template_part('partials/part-blog') ?>
		</div>
	
<?php get_footer() ?>