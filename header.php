<?php

/**

 * The header for our theme.

 *

 * Displays all of the <head> div and everything up till <div id="content">

 *

 * @package unius

 */



?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>



<head>

	<!-- <?php if(is_home()){  echo "<title>Limpeza com Zelo</title>"; } ?> -->

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!--Árquivos de css-->

	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css">

	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap-theme.min.css">

	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/mobile.css">

	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/menu-responsivo.css" >

	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/font-awesome.min.css" >

	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/vitor.css" >

	<link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200italic,700,600' rel='stylesheet' type='text/css'>
	<!--link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"-->



	





	<!--Árquivos de css-->



	<!--Árquivos de js-->
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery-2.2.3.min.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/js/jquery.bxslider.js"></script>
	<script src="http://162.144.97.31/~quitandinhaemcas/js/myJs/jmask.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/js/jquery.slicknav.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/script.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.easing.1.3.js"></script>
    <script src="http://www.limpezacomzelo.com.br/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20"></script>
	<script src="http://www.limpezacomzelo.com.br/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.4.2"></script>

	<?php wp_head(); ?>

</head>

<body>

<div itemscope itemtype="http://schema.org/DryCleaningOrLaundry">

	<meta itemprop="currenciesAccepted" content="BRL">

	<meta itemprop="paymentAccepted" content="Dinheiro">

	<meta itemprop="address" content="<?php echo strip_tags(get_option('unidade_endereco')) ?>">

	<meta itemprop="brand" content="Limpeza com Zelo <?php echo strip_tags(get_option('name_unidade')) ?>">

	<meta itemprop="department" content="Serviços de limpeza doméstico">

	<meta itemprop="award" content="Selo de Excelência em Franchising da ABF">

	<meta itemprop="telephone" content="<?php echo get_option('unidade_tel_1') ?>">

	<meta itemprop="email" content="<?php echo get_option('unidade_email') ?>">

	<meta itemprop="founder" content="Renato Ticoulat Neto">

	<meta itemprop="legalName" content="Limpeza com Zelo">

	<meta itemprop="makesOffer" content="http://www.limpezacomzelo.com.br/servicos/">

	<meta itemprop="seeks" content="http://www.limpezacomzelo.com.br/servicos/">

</div>



<!-- Facebook >

<div id="fb-root"></div>

<script>(function(d, s, id) {

		var js, fjs = d.getElementsByTagName(s)[0];

		if (d.getElementById(id)) return;

		js = d.createElement(s); js.id = id;

		js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";

		fjs.parentNode.insertBefore(js, fjs);

	}(document, 'script', 'facebook-jssdk'));</script>

< Facebook -->



<!--Google Plus-->

<script src="https://apis.google.com/js/platform.js" async defer>lang: 'pt-BR' </script>

<!--Google Plus-->



<!--Linkedi-->

<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: 'pt_BR' </script>

<!--Linkedin-->

<script>
	if (screen.width >= 1024) {
		$(document).ready(function(){
				$(window).bind('scroll', function () {
					var navHeight = 150; // custom nav height
					($(window).scrollTop() > navHeight) ? $('.menu-total').addClass('goToTop') : $('.menu-total').removeClass('goToTop');
				});
		});
	}
</script>

<header id="header">

	<?php /*if(is_home()){ ?>

					<div class="image-fundo">


						<img src="<?php echo bloginfo('template_url') ?>/img/banner-home.jpg" alt="Limpeza com Zelo" title="Limpeza com Zelo" />



						<div class="container-fluid container-faixa-azul" >

							<div class="row-fluid">	</div>

						</div>

					</div>

				<?php }*/ ?>
	<div class="menu-total">
	<div class="container-fluid top-menu">
		<div class="container social-icons">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-9 col-lg-offset-10 col-md-offset-10 div-icon">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-2 facebook-icon">
						<span class="fa-stack fa-1x">
							<a href="https://www.facebook.com/limpezacomzelo">
<!--						  <i class="fa fa-circle fa-stack-2x"></i>-->
						  <i class="fa fa-facebook fa-stack-1x "></i>
							</a>
						</span>
					</div>
					<div class="col-lg-3 col-md-3  col-sm-3  col-xs-2 facebook-icon">
						<span class="fa-stack fa-1x">
<!--						  <i class="fa fa-circle fa-stack-2x"></i>-->
						  <i class="fa fa-twitter fa-stack-1x "></i>
						</span>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3  col-xs-2 facebook-icon">
						<span class="fa-stack fa-1x">
							<a href="https://plus.google.com/b/108344415760363979105/+LimpezacomzeloBr/posts?gmbpt=true&hl=pt-BR">
<!--						  <i class="fa fa-circle fa-stack-2x"></i>-->
						  <i class="fa fa-google-plus fa-stack-1x "></i>
							</a>
						</span>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3  col-xs-2 facebook-icon">
						<span class="fa-stack fa-1x">
							<a href="https://www.youtube.com/user/limpezacomzelo">
<!--						  <i class="fa fa-circle fa-stack-2x"></i>-->
						  <i class="fa fa-youtube-play fa-stack-1x "></i>
							</a>
						</span>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="inner_header">

		<div class="container">

			<div class="row" id="topo-header">

				<div class="col-md-3">

					<div id="logo">


						<?php if(is_home()){ ?>


							<h1>

								<a href="<?php echo esc_url( home_url( '/' ) ); ?>">

									<img src="<?php bloginfo('template_directory')?>/img/logo-limpeza-com-zelo.png" alt="Limpeza com Zelo" title="Limpeza com Zelo" />

								</a>

							</h1>


						<?php }else{ ?>



							<div class="container-logo">

								<a href="<?php echo esc_url( home_url( '/' ) ); ?>">

									<img src="<?php bloginfo('template_directory')?>/img/logo-limpeza-com-zelo.png" alt="Limpeza com Zelo" title="Limpeza com Zelo" />

								</a>

							</div>



						<?php } ?>

					</div>

				</div>

				<div class="col-md-9 container-menu">

					<nav>

						<div class="menu">

							<?php /*template do menu*/ get_template_part('partials/part-menu'); ?>

						</div>

					</nav>

					<!--<div class="sebrae">

									<a href="http://www.compredopequeno.com.br/empresa/limpeza-com-zelo-franquias-35726" target="_blank">

										<img src="<?php bloginfo('url')?>/wp-content/uploads/2015/09/novo-logo-sebrae.png" class="img-responsive">

									</a>

								</div>-->

				</div>





			</div>

		</div>

	</div>
	</div>
	<?php if(is_home()){ ?>
		<div class="container-fluid banner-home" style="margin-bottom: -200px; background-image: url('<?php bloginfo('template_directory')?>/img/banner-novo2.jpg');background-size: cover;min-height: 660px;    	 margin-top: 215px;
			z-index: -90000;
			">
			<div class='row'>

				<div class='container texts-and-buttons'>
					<div class='col-md-12'>

						<div class='text-center new-title title-home'>
							<h2>Abra as portas para
								a <span>Limpeza</span>!</h2>
						</div>


					</div>

					<div class='col-md-12'>
						<div class="sub-new-title">
							<p>Escolha o tipo de limpeza conforme a sua necessidade:</p>
						</div>
					</div>
					<div class="col-md-10 col-md-offset-1" >
					<div class="col-md-6">
						<div class="orcar-servico-limpeza">
							<a href="/servicos"><br>
								<ul>
									<li>Serviços Diversificados <span class="circle">&bull;</span></li>
									<li>Orçamentos Personalizado <span class="circle">&bull;</span></li>
									<li>Atendimento realizado por <span class="circle"> &bull;</span><br><span style="margin-right:18px"> 2 pessoas </span> </li>
								</ul>
								<br>
							<div class="green-button">
								<p style="background-color:#6BC7C1">Orçar Serviços de Limpeza</p>
							</div>
							</a>
						</div>
					</div>

					<div class='col-md-6' >
						<div class="orcar-servico-hora">
							<a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1/"><br>
							<ul>
								<li><span class="circle">&bull;</span> Limpeza Geral</li>
								<li><span class="circle">&bull;</span> Contratação Online</li>
								<li><span class="circle">&bull;</span> Atendimento realizado por <br> <span style="margin-left:15px"> 1 pessoa</span> </li>
							</ul>
							<br>
							<div class="pink-button">
								<p>Orçar Limpeza por hora</p>
							</div>
							</a>
						</div>
					</div>
					</div>

					<div class='col-md-9'></div>

				</div>

			</div>
			<div class="container">
			<div class="row">

			</div>
			</div>
		</div>
	<?php } ?>


</header>

<?php

$slug = '';
$nameAttribute = "";
if (is_category( )) {

	$cat = get_query_var('cat');

	$yourcat = get_category ($cat);

	$slug = $yourcat->slug;

	$nameAttribute = "name='".$slug."'";

}

?>

<!-- name="<?php echo $slug ?>" -->

<div id="content" class="site-content" <?php echo $nameAttribute ?> >



	<?php if(!is_home()){ ?>



		<div class="container">

			<div class="row">

				<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">

					<!-- <ol class="breadcrumb">

                      <li><a href="#">Home</a></li>

                      <li class="active">Data</li>

                    </ol> -->

					<?php if ( function_exists('yoast_breadcrumb') ) {

						yoast_breadcrumb('<p id="breadcrumbs">','</p>');

					} ?>



				</div>

			</div>

		</div>

	<?php } ?>
