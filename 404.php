<?php get_header(); ?>

<div class="container-fluid container-404">
	<div class="row">
		<div class="container">
			
			<div class="row">
				<div class="col-md-12 container-label-404">
					<h3 class="label-404">404 - Página não encontrada</h3>
					<h4 class="label-msg">Parece que não foi possível encontrar o que você está buscando.</h4>
				</div>
			</div>
			
			<div class="col-md-12 container-form-search-404">
				<div class="form-search-404">
					<?php get_search_form() ?>
				</div>	
			</div>
		</div>
	</div>	
</div>

<?php get_footer(); ?>

