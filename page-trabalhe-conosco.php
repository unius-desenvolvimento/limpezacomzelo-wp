<?php get_header(); ?>

<div class="container content-4 title-trabalhe-conosco">

	<div class="row">
		<div class="col-md-12">
			<h2 class="page-title4">TRABALHE CONOSCO</h2>
				<p class="page-subtitle"></p>
		</div>
	</div>

</div>

<div class="container-fluid content-4 trabalhe-conosco">
		<div class="row">
			<div class="container">

				<div class="row">
					<div class="col-md-12 block-form-cont-4">
						<?php echo do_shortcode('[contact-form-7 id="429" title="Trabalhe Conosco" html_class="use-floating-validation-tip"]'); ?>
					</div>
				</div>

			</div>
		</div>
</div>


<?php get_footer(); ?>