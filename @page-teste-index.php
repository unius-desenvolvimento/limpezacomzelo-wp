<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package unius
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="inner_content">
				<!--CONTENT 1 HOME/VANTAGENS-->
					<?php get_template_part('partials/part-vantagens') ?>

				<!--CONTENT 2-->
					<?php get_template_part('partials/part-bem_vindo') ?>

				<!--CONTENT 3 SERVICO HOME-->
					<?php get_template_part('partials/part-servico_home') ?>

				<!--CONTENT 8 MIDIA-->
					<?php get_template_part('partials/part-midia') ?>


				<!--CONTENT 4 FORM ORÇAMENTO-->
					<?php get_template_part('partials/part-teste-form_orcamento') ?>

				<!--CONTENT 5 DEPOIMENTOS-->
					<?php get_template_part('partials/part-depoimentos') ?>

				<!--CONTENT 6 HOME/UNIDADES -->
					<?php get_template_part('partials/part-unidades') ?>

				<!--CONTENT 7 BLOG-->
					<?php get_template_part('partials/part-blog') ?>
			</div>
		</main>
	</div>


<?php get_footer(); ?>
