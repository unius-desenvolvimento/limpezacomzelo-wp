<div class="container-fluid content-8">
	
	<div class="row">
		<div class="container">
			<div class="row">
				<?php 
					$qry = new WP_Query(array('post_type' => 'midia','posts_per_page' => 1,
							'tax_query' => array(
										array(
											'taxonomy' => 'categorias-midia',
											'field'    => 'slug',
											'terms'    =>  'cms-na-midia',
										),)
						));
					
					if($qry->have_posts()){	
						
						while($qry->have_posts()){
							$qry->the_post();
				?>
				
						<div class="col-md-12">
							<h2 class="page-title4 title-page"><?php echo the_title() ?></h2>
							<p class="page-subtitle"></p>
						</div>
						
						<div class="col-md-12">
							<article class="article-midia-home">
								<h2 class="article-header"><?php echo the_title() ?></h2>
								
								<div class="conteudo-midia-home">
								 	<?php the_excerpt() ?>
								</div>
							</article>
						</div>
					
					<?php }}else{ ?>
						
						<div class="col-md-12">
							<article class="article-midia-home">
								<span>Nenhum post encontrado</span>
							</article>
						</div>
						
					<?php } ?>
				
			</div>
		</div>
	</div>	
	
	<div class="row">
		
		<div class="col-md-12 banner-na-midia">
			<div class="slider2">
			
				<?php 
					$qry = new WP_Query(array('post_type' => 'midia','posts_per_page' => 100,
							'tax_query' => array(
										array(
											'taxonomy' => 'categorias-midia',
											'field'    => 'slug',
											'terms'    =>  'limpeza-com-zelo-na-midia',
										),)
						));
						
					while($qry->have_posts()){
						$qry->the_post();
				?>
				
						  <figure class="col-md-3 slider-midia">
						  	<a href="<?php the_permalink() ?>" title="<?php echo get_the_title() ?>">
						  		<?php the_post_thumbnail('thumbnail-midia', array( 'alt' =>  get_the_title(), 'title' => get_the_title() )) ?>
						  	</a>
						  	<figcaption>
						  		<?php the_excerpt() ?>
						  		<span class="mes-midia"><?php echo  get_the_date('F Y') ?></span>
						  		<span class="title-midia"><?php echo the_title() ?></span>
						  	</figcaption>
						  </figure>
				  
			  	<?php } ?>
			  
			</div>
		</div>
		
	</div>	
			
		
	
</div>