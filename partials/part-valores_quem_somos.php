<div class="container">
	<div class="sub-container-index">
		<div class="row">
			<div class="col-md-12">
				<div class="block-home">
					<h2 class="page-title3"><?php echo get_option('valores_quem_somos') ?></h2>
					<p class="page-subtitle"></p>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="container-circle">
				
				<?php 
					$qry = new WP_Query(array( 'post_type' => 'vantagens', 'showposts' => 3, 
							'tax_query' => array(
									array(
										'taxonomy' => 'categorias-vantagens',
										'field'    => 'slug',
										'terms'    =>  'quem-somos',
									),)));
					
					if($qry->have_posts())
					{	
						while($qry->have_posts())
						{
							$qry->the_post();
				?>
							<div class="col-md-4">
								<div class="block-circle circle-1">
									<?php the_post_thumbnail('thumbnail-vantagens-valores', array( 'alt' =>  get_the_title(), 'title' => get_the_title() )) ?>
								</div>
								<div class="block-txt-circle">
									<div>
										<h3><?php the_title() ?></h3>
										<?php the_content() ?>
									</div>
								</div>
							</div>
				
				<?php } }else{ ?>				
							<div class="col-md-4">
								<span>Nenhum vantagem informada</span>			
							</div>
				<?php } ?>
			
			</div>
		</div>
	</div>
</div>