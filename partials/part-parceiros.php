<div class="container-fluid parceiro">
	<div class="row">
		<div class="container parceiro-inside ">
			<div>
				<div class="row">
					<div class="col-md-12" style="padding: 1%">
						<div class="col-md-2 col-sm-6 col-xs-12">
							<div class="parceiro-logo">
								<img alt="Limpeza com Zelo na UOL" title="Limpeza com Zelo na UOL" src="<?php bloginfo('template_directory')?>/img/uol.png" >
							</div>
						</div>
						<div class="col-md-2 col-sm-6 col-xs-12">
							<div class="parceiro-logo">
								<img alt="Limpeza com Zelo na Veja" title="Limpeza com Zelo na Veja" src="<?php bloginfo('template_directory')?>/img/veja.png" >
							</div>
						</div>
						<div class="col-md-2 col-sm-6 col-xs-12">
							<div class="parceiro-logo">
								<img alt="Limpeza com Zelo no O Globo" title="Limpeza com Zelo no O Globo" src="<?php bloginfo('template_directory')?>/img/globo.png" >
							</div>
						</div>
						<div class="col-md-2 col-sm-6 col-xs-12">
							<div class="parceiro-logo">
								<img alt="Limpeza com Zelo no Estadão" title="Limpeza com Zelo no Estadão" src="<?php bloginfo('template_directory')?>/img/estadao.png" >
							</div>
						</div>
						<div class="col-md-2 col-sm-6 col-xs-12">
							<div class="parceiro-logo">
								<img alt="Limpeza com Zelo no Pequenas Empresas, Grande Negócios" title="Limpeza com Zelo no Pequenas Empresas, Grande Negócios" src="<?php bloginfo('template_directory')?>/img/pequenasempresas.png" >
							</div>
						</div>
						<div class="col-md-2 col-sm-6 col-xs-12">
							<div class="parceiro-logo">
								<img alt="Limpeza com Zelo na ABF" title="Limpeza com Zelo na ABF" src="<?php bloginfo('template_directory')?>/img/abf.png" >
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
