<!-- Related Products -->
        <?php
            $args=array(
                'post_type' => get_post_type(),
                'post__not_in' => array($post->ID),
                'orderby' => 'rand',
                'showposts'=>3, // Number of related posts that will be shown.
                'ignore_sticky_posts'=>1
            );
            
            $my_query = new wp_query($args);
		?>
<div class="container-fluid content-7">
	<div class="row">
		
		<div class="container">
			
			<div class="row">
				<div class="col-md-12">
					<h2 class="page-title4"><?php echo get_option('post_relacionados_unidade') ?></h2>
					<p class="page-subtitle"></p>
				</div>
			</div>
	
			<div class="row">
				
		          <?php if( $my_query->have_posts() ) { ?>	
		        
		                
			                <?php 
			                    while ($my_query->have_posts()) 
			                    {
			                        $my_query->the_post();
			                        $category = get_the_category();
							?>
									
								<div class="col-md-4 col-sm-4 col-xs-12 cont-blog">
									<figure>
										<a href="<?php  echo get_permalink() ?>" title="<?php  echo get_the_title() ?>">
											<span class="glyphicon glyphicon-share-alt"></span>
											<?php the_post_thumbnail('thumb_blog_home', array( 'alt' =>  get_the_title(), 'title' => get_the_title() )) ?>
										</a>
										
										<figcaption>
											<a href="<?php  echo get_permalink() ?>" title="<?php echo get_the_title() ?>">
												<h3>
													<?php  echo the_title() ?>
												</h3>
											</a>	
											<?php echo the_excerpt() ?>
											<a href="<?php the_permalink() ?>" class="more-blog" title="Lêr mais sobre <?php echo get_the_title() ?>">Leia Mais</a>
										</figcaption>
									</figure>
								</div>	
								
			        		<?php } ?>
			        		
			        		
		        	<?php } ?>		
		        	
		        	<?php wp_reset_query(); ?>
		        	
       		</div>
        </div>	
    </div>     	
</div>        	