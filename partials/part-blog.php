<div class="container-fluid content-7">
	<div class="row">
		
		<div class="container">
			
			<div class="row">
				<div class="col-md-12">
					<h2 class="page-title4"><?php echo get_option('blog_home') ?></h2>
					<p class="page-subtitle"></p>
				</div>
			</div>
			
			<div class="row">
				
				<?php 
					$qry = new WP_Query(array('post_type' => 'post', 'showposts' => 3));
					
					if($qry->have_posts())
					{	
						while($qry->have_posts())
						{
							$qry->the_post();
				?>
							<div class="col-md-4 col-sm-4 col-xs-12 cont-blog">
								<figure>
									<a href="<?php the_permalink() ?>" title="<?php get_the_title() ?>">
										<span class="glyphicon glyphicon-share-alt"></span>
										<?php the_post_thumbnail('thumb_blog_home', array( 'alt' =>  get_the_title(), 'title' => get_the_title() )) ?>
									</a>
									
									<figcaption>
										<a href="<?php the_permalink() ?>" title="<?php get_the_title() ?>">
											<h3>
												<?php the_title() ?>
											</h3>
										</a>	
										<?php the_excerpt() ?>
										<a href="<?php the_permalink() ?>" class="more-blog" title="Lêr mais sobre <?php echo get_the_title() ?>">Leia Mais</a>
									</figcaption>
								</figure>
							</div>
				
			  	<?php } }else{ ?> 
			  
			  				<div class="col-md-4 col-sm-4 col-xs-12 cont-blog">
								<span>Nenhum post encontrado</span>
							</div>
			  <?php } ?>
				
			</div>	
			
		</div>		
	</div>
</div>