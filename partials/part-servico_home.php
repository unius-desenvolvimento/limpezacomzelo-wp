<div class="container-fluid content-3">
    <div class="row">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title4"><?php echo get_option('servico_home') ?></h2>
                <p class="page-subtitle"></p>
            </div>
            <div class="col-md-12">
                <div class="container field-text">
                    Desenvolvemos dois sistemas de limpeza para atendê-los deixando sua residência limpa e higienizada de forma rápida e econômica.
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="row servicos-home">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 no-paddin">
            <div class="img first">
                <img alt="Logo Limpeza com Zelo" title="Logo Limpeza com Zelo" src="<?php bloginfo('template_directory')?>/img/logo-limpeza-por-zelo.png" class="img-responsive" >
                <div class="text">
                    <p style="text-align: justify; font-size: 18px">
                        Quando você contrata um serviço da Limpeza com Zelo você é atendido por um franqueado autorizado a fazer
                        diversos tipos de serviços, em algumas regiões definidas em Unidades.<br><br>

                        O franqueado vai até a sua residência para avaliar o que precisa ser feito, determinando qual tipo de serviço
                        é o mais adequado para a suas necessidades e faz um orçamento personalizado.<br><br>

                        Após a aprovação do orçamento, uma equipe com 2 funcionários faz a limpeza no dia e horário previamente combinado.
                        A Limpeza com Zelo oferece os produtos e equipamentos necessários para o serviço.
                    </p>
                    <a href="<?php bloginfo('url')?>/servicos" class="servico">
                        <div class="green-button">
                            <p>ORÇAR SERVIÇOS</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 no-paddin">
            <div class="img last">
                <img alt="Logo Limpeza por Hora" title="Logo Limpeza por Hora" src="<?php bloginfo('template_directory')?>/img/logo-limpeza-por-hora.png" class="img-responsive" >
                <div class="text">
                    <p style="text-align: justify; font-size: 18px">
                        Quando você contrata um serviço da Limpeza por Hora você é atendido por uma franqueada autorizada a fazer
                        pessoalmente uma limpeza geral, lavar roupa, passar roupa ou lavar louça de sua residência, de acordo com
                        a sua escolha.<br><br>

                        O serviço de limpeza geral será realizado durante o período de tempo pré-determinado por você na hora da
                        contratação no site Limpeza por Hora. O agendamento, a contratação e o pagamento são feitos online.<br><br>

                        O serviço está disponível apenas para a cidade de São Paulo e franqueada leva todos os produtos e equipamentos
                        necessários.
                    </p>
                    <a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1" class="servico">
                        <div class="pink-button">
                            <p>ORÇAR LIMPEZA</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

