<div class="container content2-home">
	<div class="row">

		<?php
			$qry = new WP_Query(array('post_type' => 'bem_vindo', 'showposts' => 1));

			if($qry->have_posts()){
				while($qry->have_posts())
				{ $qry->the_post();
		?>
					<div class="col-md-12">
						<h2 class="page-title-2-home page-title3"><?php the_title() ?></h2>
					</div>

					<div class="col-md-12">

						<div class="sub-text">
							<?php the_content() ?>
						</div>

						<figure>
							<?php the_post_thumbnail('', array( 'alt' =>  get_the_title(), 'title' => get_the_title() )) ?>
						</figure>

					</div>

		<?php }}else{ ?>
					<div class="col-md-12"> <span>Nenhum post encontrado</span></div>
		<?php } ?>
		<?php ?>

	</div>
</div>
