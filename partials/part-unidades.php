<?php 
	
	/*ARGUMENTOS PARA RETORNAR AS CATEGORIAS(ESTADOS) DAS UNIDADES*/
		$args = array(
		 'type'                     => 'unidades',
		 'child_of'                 => 0,
		 'parent'                   => 0,
		 'orderby'                  => get_field('order'),
		 'order'                    => 'ASC',
		 'hide_empty'               => 0,
		 'hierarchical'             => 1,
		 'exclude'                  => '12',
		 'include'                  => '',
		 'number'                   => '',
		 'taxonomy'                 => 'categorias-unidades',
		 'pad_counts'               => false 
	
		);
		
	/*PEGANDO AS CATEGORIAS FILTRADAS DE ACORDO COM OS ARGUMENTOS ACIMA*/
		$categories = get_categories($args);
?>

<div class="container content-6 unidades_home">
	<div class="row">
		
		<div class="col-md-12">
			<h2 class="page-title4"><?php echo get_option('unidades_home') ?></h2>
			<p class="page-subtitle"></p>
		</div>
		
		<?php foreach($categories as $category){ ?>
			
				<section class="col-md-4 col-sm-3">
					<h3><?php echo $category->name; ?></h3>
					<ul>
						
						<?php  
						  $query = new WP_Query( array(
						  					"post_type" 	=> 'unidades', 
						  					'order' => 'ASC',
						  					'orderby' => 'name',
						  					'showposts' => 100,
						  					'tax_query' => array(
												array(
													'taxonomy' => 'categorias-unidades',
													'field'    => 'slug',
													'terms'    =>  $category->slug,
												),))); 
						  ?>
						  				
						  <?php while($query->have_posts()) { $query->the_post();  ?>
									<li>
										<h4>
											<a href="<?php echo get_protocol(); the_field('linkunidade') ?>" title="UNIDADE LIMPEZA COM ZELO <?php the_title() ?>" target="_self">
												<?php the_title() ?>
											</a>
										</h4>	
									</li>
						<?php } ?>	
							
					</ul>
				</section>
			
		<?php } ?>
		
	</div>
</div>