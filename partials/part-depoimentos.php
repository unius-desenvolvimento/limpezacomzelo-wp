<div class="container-fluid content-5">
	<div class="row">
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="page-title4"><?php echo get_option('depoimentos_home') ?></h2>
					<p class="page-subtitle"></p>
				</div>
				
				
				<div class="col-md-2 visible-md visible-lg quote">
					<i class="fa fa-quote-left"></i>
				</div>
				
				<div class="col-md-10 container-depoimentos">
					<div class="bxslider2">
					
						<?php 
							$qry = new WP_Query(array('post_type' => 'depoimentos','posts_per_page' => 100));
							
							if($qry->have_posts()){
								while($qry->have_posts())
								{ $qry->the_post();
						?>
									<div class="field-slider">
								  		<?php the_content() ?>
								  		<h4><?php the_title() ?></h4>
								  	</div>
						  
				  	    <?php  } 	}else{ ?>
				  	    			
				  	    			<div class="field-slider">
								  		<p>Nenhum comentário encontrado</p>
								  		<h4>Administrador</h4>
								  	</div>
				  	    
					    <?php }	?>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>