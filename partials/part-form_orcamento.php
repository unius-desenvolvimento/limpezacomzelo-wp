<div class="container-fluid orcamento-form" id="orcamento">
	<div class="container content-4">
		<div class="row">

			<div class="col-md-12">
				<h2 class="page-title4"><?php echo get_option('form_title_orcamento_home'); ?> </h2>
				<p class="page-subtitle"></p>
				<div class="sub-text">
				<p>

				</p>

				</div>
			</div>

			<div class="col-md-12 p-txt">
				<p><?php echo get_option('form_content_orcamento_home') ?></p>
			</div>

			<div class="col-md-12 block-form-cont-4" id="limpezacomzelo-form">

				<?php echo do_shortcode('[contact-form-7 id="12" title="SOLICITE UM ORÇAMENTO" html_class="use-floating-validation-tip"]'); ?>
			</div>

			<div class="col-md-12 block-form-cont-4" id="limpezaporhora-form">

				<?php echo do_shortcode('[contact-form-7 id="906" title="SOLICITE UM ORÇAMENTO LIMPEZA POR HORA" html_class="use-floating-validation-tip"]'); ?>
			</div>
		</div>

		<!-- Mensagem OK da limpeza -->
		<div class="row">
			<div class="col-md-12 sent-ok-limpezacomzelo">

				<div class="row title-top">
					<div class="col-md-2 smile">
						<i class="fa fa-smile-o" aria-hidden="true"></i>
					</div>
					<div class="col-md-8">
						<h3>
							Muito obrigado por sua escolha
						</h3>
					</div>
					<div class="col-md-2 smile">
						<i class="fa fa-smile-o" aria-hidden="true"></i>
					</div>
				</div>

				<div class="row content-body">

					<div class="col-md-12 text-center">
						<img src="<?php echo get_template_directory_uri() ?>/img/logo-redondo-zelo.png" class="img-responsive">
					</div>

				</div>

				<div class="row title-bottom">
					<div class="col-md-12 text-center">
						<h3>Em breve, nossa equipe entrará em contato !</h3>
					</div>
				</div>

			</div>
		</div>
		<!-- Mensagem OK da limpeza -->


		<!-- Mensagem OK da limpeza porhora-->
		<div class="row">
			<div class="col-md-12 sent-ok-limpezaporhora">

				<div class="row title-top">
					<div class="col-md-2 smile">
						<i class="fa fa-smile-o" aria-hidden="true"></i>
					</div>
					<div class="col-md-8">

						<p>
							A <strong>Limpeza com Zelo</strong> não atende a sua região
						</p>
						<p>
							Mas a <b>boa notícia</b> é que agora nós
						</p>
						<p>
							temos a <strong>Limpeza por hora</strong> para atender
						</p>

					</div>
					<div class="col-md-2 smile">
						<i class="fa fa-smile-o" aria-hidden="true"></i>
					</div>
				</div>

				<div class="row content-body">

					<div class="col-md-12 text-center">
						<img src="<?php echo get_template_directory_uri() ?>/img/logo-redondo-hora.png" class="img-responsive">
					</div>

				</div>

				<div class="row title-bottom">
					<div class="col-md-12 text-center">
						<p>Mais agilidade para:
							<ul class="services-list">
								<li>Orçar</li>
								<li>Contratar</li>
								<li>Agendar</li>
								<li>Pagar</li>
							</ul>
						</p>
						<p>Tudo online para facilitar a sua vida e garantir a Limpeza da sua casa</p>
						<p class="text-center ">
							<a class="button-orcar btn" href=" http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1" target="_blank">ORÇAR LIMPEZA JÁ</a>
						</p>
					</div>
				</div>

			</div>
		</div>
		<!-- Mensagem OK da limpeza -->

	</div>
</div>
<script>
	/*formularios new*/


	function populaDadosInForms(origin, dest){
		//Pego os dados do origin, adiciona para o dest
		var value;
		value = $(origin).find("input[name='email']").val();
		$(dest).find("input[name='email']").val(value);

		value = $(origin).find("input[name='nome']").val();
		$(dest).find("input[name='nome']").val(value);

		value = $(origin).find("input[name='telefone']").val();
		$(dest).find("input[name='telefone']").val(value);

		value = $(origin).find("input[name='celular']").val();
		$(dest).find("input[name='celular']").val(value);

		value = $(origin).find("input[name='cep']").val();
		$(dest).find("input[name='cep']").val(value);

		value = $(origin).find("input[name='endereco']").val();
		$(dest).find("input[name='endereco']").val(value);

		value = $(origin).find("input[name='numero']").val();
		$(dest).find("input[name='numero']").val(value);

		value = $(origin).find("input[name='bairro']").val();
		$(dest).find("input[name='bairro']").val(value);

		value = $(origin).find("input[name='cidade']").val();
		$(dest).find("input[name='cidade']").val(value);

		value = $(origin).find("input[name='estado']").val();
		$(dest).find("input[name='estado']").val(value);

	}

	$(document).ready(function(){



		var regexString  = /(JARDIM SAÚDE|SAÚDE|SANTA CRUZ|CHACARA KLABIN|SACOMÃ|VILA MASCOTE|BROOKLIN NOVO E VELHO|VILA CORDEIRO|VILA CRUZEIRO|CHÁCARA SANTO ANTÔNIO|VILA OLÍMPIA|INDIANÓPOLIS|PINHEIROS|VILA LEOPODINA|LAPA|VILA MADALENA|ALTO DE PINHEIROS|SUMARÉ|VILA ROMANA|PIRITUBA|SANTANA|VILA GUILHERME|CASA VERDE|LIMÃO|VILA MARIA|VILA PAIVA|SANTA TEREZINHA|JARDIM ANDARAÍ|IMIRIM|MANDAQUI|TUCURUVI|VILA NOVA MAZZEI|TREMEMBÉ|LAUZANE PAULISTA|JARDIM BRASIL|VILA MEDEIROS|ARTHUR ALVIM|TATUAPÉ|PENHA|CIDADE PATRIARCA|VILA MATILDE|VILA RÉ|GUAIANAZES|VILA CARMOSINA|MOOCA|VILA CARRÃO|BELÉM|BRÁS|ARICANDUVA)/;

		$("#limpezaporhora-form").hide();
		$("#limpezacomzelo-form select").change(

			function (){


				var option = $(this).find('option:selected').val();

				var result = option.match(regexString);

				//Se o result não for null, significa que ele escolheu umas das opções da limpeza por hora
				if(result != null){
					//LIMPEZA POR HORA



					//Aplica a logica somente se o form estiver invisivel
					if($("#limpezaporhora-form").css("display") != "block"){
						$("#limpezaporhora-form select").val( $("#limpezacomzelo-form select option:selected").val());
						$("#limpezaporhora-form select").val( $("#limpezacomzelo-form select option:selected").val());

						populaDadosInForms("#limpezacomzelo-form", "#limpezaporhora-form");
					}

					$("#limpezacomzelo-form").hide();
					$("#limpezaporhora-form").show();

				}else {
					//LIMPEZA COM ZELO

					//Aplica a logica somente se o form estiver invisivel
					if($("#limpezacomzelo-form").css("display") != "block"){


						$("#limpezacomzelo-form select").val( $("#limpezaporhora-form select option:selected").val());
						$("#limpezacomzelo-form select").val( $("#limpezaporhora-form select option:selected").val());

						populaDadosInForms("#limpezaporhora-form", "#limpezacomzelo-form");

					}


					$("#limpezacomzelo-form").show();
					$("#limpezaporhora-form").hide();


//				$("#limpezacomzelo-form select").val($("#limpezaporhora-form select option:selected").val());
				}


		});
		$("#limpezaporhora-form select").change(
			function (){


				var option = $(this).find('option:selected').val();

				var result = option.match(regexString);

				//Se o result não for null, significa que ele escolheu umas das opções da limpeza por hora
				if(result != null){
					//LIMPEZA POR HORA
					console.log("LIMPEZA Por hora");


					//Aplica a logica somente se o form estiver invisivel
					if($("#limpezaporhora-form").css("display") != "block"){
						$("#limpezaporhora-form select").val( $("#limpezacomzelo-form select option:selected").val());
						$("#limpezaporhora-form select").val( $("#limpezacomzelo-form select option:selected").val());

						populaDadosInForms("#limpezacomzelo-form", "#limpezaporhora-form");
					}

					$("#limpezacomzelo-form").hide();
					$("#limpezaporhora-form").show();

				}else {
					//LIMPEZA COM ZELO

					console.log("LIMPEZA COM ZELO");
					//Aplica a logica somente se o form estiver invisivel
					if($("#limpezacomzelo-form").css("display") != "block"){


						$("#limpezacomzelo-form select").val( $("#limpezaporhora-form select option:selected").val());
						$("#limpezacomzelo-form select").val( $("#limpezaporhora-form select option:selected").val());

						populaDadosInForms("#limpezaporhora-form", "#limpezacomzelo-form");
					}


					$("#limpezacomzelo-form").show();
					$("#limpezaporhora-form").hide();


//				$("#limpezacomzelo-form select").val($("#limpezaporhora-form select option:selected").val());
				}

			}

		);

	});

		$(document).on('change',$('#estados'),function(){

			var value = $( "#estados option:selected",this).val();
			$('#' + value).css('display', 'block');
			$('.unidade').val($('#' + value).eq(0).val());
		});
		$('.unidade ').css('display', 'none');





</script>




