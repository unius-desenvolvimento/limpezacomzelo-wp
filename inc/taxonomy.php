<?php
	
	



/*TAXONOMY UNIDADES*/
	function create_unidades_taxonomies() {
			$labels = array(
					'name' => _x( 'Categorias das unidades', 'taxonomy general name' ),
					'singular_name' => _x( 'Categoria da unidades', 'taxonomy singular name' ),
					'search_items' =>  __( 'Buscar Categorias' ),
					'popular_items' => __( 'Categorias Populares' ),
					'all_items' => __( 'Todas as Categorias' ),
					'parent_item' => null,
					'parent_item_colon' => null,
					'edit_item' => __( 'Editar Categoria' ),
					'update_item' => __( 'Atualizar Categoria' ),
					'add_new_item' => __( 'Adicionar Nova Categoria' ),
					'new_item_name' => __( 'Nome da Nova Categoria' ),
					'separate_items_with_commas' => __( 'Separe as Categorias por Virgulas' ),
					'add_or_remove_items' => __( 'Adicionar ou Remover Categorias' ),
					'choose_from_most_used' => __( 'Escolha as Categorias Mais Utilizadas' ),
					'menu_name' => __( 'Categorias' ),
			);
			register_taxonomy('categorias-unidades','galeria-de-unidades',array(
					'labels' => $labels,
					'show_ui' => true,
					'update_count_callback' => '_update_post_term_count',
					'rewrite' => array('slug' => 'categorias-unidades'),
					'hierarchical' => true,
			));
	}
	add_action( 'init', 'create_unidades_taxonomies', 0 );

/*TAXONOMY UNIDADES*/
	function create_servicos_taxonomies() {
			$labels = array(
					'name' => _x( 'Categorias dos serviços', 'taxonomy general name' ),
					'singular_name' => _x( 'Categoria do serviço', 'taxonomy singular name' ),
					'search_items' =>  __( 'Buscar Categorias' ),
					'popular_items' => __( 'Categorias Populares' ),
					'all_items' => __( 'Todas as Categorias' ),
					'parent_item' => null,
					'parent_item_colon' => null,
					'edit_item' => __( 'Editar Categoria' ),
					'update_item' => __( 'Atualizar Categoria' ),
					'add_new_item' => __( 'Adicionar Nova Categoria' ),
					'new_item_name' => __( 'Nome da Nova Categoria' ),
					'separate_items_with_commas' => __( 'Separe as Categorias por Virgulas' ),
					'add_or_remove_items' => __( 'Adicionar ou Remover Categorias' ),
					'choose_from_most_used' => __( 'Escolha as Categorias Mais Utilizadas' ),
					'menu_name' => __( 'Categorias' ),
			);
			register_taxonomy('categorias-servicos','galeria-de-servicos',array(
					'labels' => $labels,
					'show_ui' => true,
					'update_count_callback' => '_update_post_term_count',
					'rewrite' => array('slug' => 'categorias-servicos'),
					'hierarchical' => true,
			));
	}
	add_action( 'init', 'create_servicos_taxonomies', 0 );


/*TAXONOMY MIDIA*/
	function create_midia_taxonomies() {
			$labels = array(
					'name' => _x( 'Categorias da midia', 'taxonomy general name' ),
					'singular_name' => _x( 'Categoria da midia', 'taxonomy singular name' ),
					'search_items' =>  __( 'Buscar Categorias' ),
					'popular_items' => __( 'Categorias Populares' ),
					'all_items' => __( 'Todas as Categorias' ),
					'parent_item' => null,
					'parent_item_colon' => null,
					'edit_item' => __( 'Editar Categoria' ),
					'update_item' => __( 'Atualizar Categoria' ),
					'add_new_item' => __( 'Adicionar Nova Categoria' ),
					'new_item_name' => __( 'Nome da Nova Categoria' ),
					'separate_items_with_commas' => __( 'Separe as Categorias por Virgulas' ),
					'add_or_remove_items' => __( 'Adicionar ou Remover Categorias' ),
					'choose_from_most_used' => __( 'Escolha as Categorias Mais Utilizadas' ),
					'menu_name' => __( 'Categorias' ),
			);
			register_taxonomy('categorias-midia','galeria-de-midia',array(
					'labels' => $labels,
					'show_ui' => true,
					'update_count_callback' => '_update_post_term_count',
					'rewrite' => array('slug' => 'categorias-midia'),
					'hierarchical' => true,
			));
	}
	add_action( 'init', 'create_midia_taxonomies', 0 );

/*TAXONOMY MIDIA*/
	function create_vatagens_taxonomies() {
			$labels = array(
					'name' => _x( 'Categorias dos posts', 'taxonomy general name' ),
					'singular_name' => _x( 'Categoria do post', 'taxonomy singular name' ),
					'search_items' =>  __( 'Buscar Categorias' ),
					'popular_items' => __( 'Categorias Populares' ),
					'all_items' => __( 'Todas as Categorias' ),
					'parent_item' => null,
					'parent_item_colon' => null,
					'edit_item' => __( 'Editar Categoria' ),
					'update_item' => __( 'Atualizar Categoria' ),
					'add_new_item' => __( 'Adicionar Nova Categoria' ),
					'new_item_name' => __( 'Nome da Nova Categoria' ),
					'separate_items_with_commas' => __( 'Separe as Categorias por Virgulas' ),
					'add_or_remove_items' => __( 'Adicionar ou Remover Categorias' ),
					'choose_from_most_used' => __( 'Escolha as Categorias Mais Utilizadas' ),
					'menu_name' => __( 'Categorias' ),
			);
			register_taxonomy('categorias-vantagens','galeria-de-vantagens',array(
					'labels' => $labels,
					'show_ui' => true,
					'update_count_callback' => '_update_post_term_count',
					'rewrite' => array('slug' => 'categorias-vantagens-valores'),
					'hierarchical' => true,
			));
	}
	add_action( 'init', 'create_vatagens_taxonomies', 0 );

