<?php

	/*ADD SUPPORT SECUNDARIA THUMBNAIL */
		if (class_exists('MultiPostThumbnails')) {
		    new MultiPostThumbnails(
		        array(
		            'label' => 'Imagem Destacada 2',
		            'id' => 'secondary-image',
		            'post_type' => 'servicos'
		        )
		    );
		}
		
	/*CHAMAR SEGUNDA THUMBNAIL DO POST*/	
		function get_the_post_thumbnail_secundary(){
			if (class_exists('MultiPostThumbnails')) {
			    MultiPostThumbnails::the_post_thumbnail(
			        get_post_type(),
			        'secondary-image'
			    );
			}
		}
	
	/*PEGAR PROTOCOLO*/
		
		function get_protocol(){
			$protocolo = (strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === false) ? 'http://' : 'https://';
			return $protocolo;
		}
			
		
	/*SUPPORT IMAGE SIZE BLOG HOME*/
		add_image_size('thumb_blog_home', 262, 262, true);
	
	/*SUPPORT IMAGE SIZE BEM VINDO HOME*/
		add_image_size('thumb_bem_vindo_home', 746, 466, true);
		
	/*SUPPORT IMAGE SIZE SINGLE BLOG*/
		add_image_size('thumbnail-single', 663, 221, true);	

	/*SUPPORT IMAGE SIZE NA MÍDIA HOME*/
		add_image_size('thumbnail-midia', 537, 339, true);	

	/*SUPPORT IMAGE SIZE NA ICON SERVIÇOS*/
		add_image_size('thumbnail-servicos', 141, 141, true);	

	/*SUPPORT IMAGE SIZE NA VANTAGENS/VALORES*/
		add_image_size('thumbnail-vantagens-valores', 189, 189, true);	
