<?php

	//Seta os dados no banco
		function up_del_new_field($field)
		{
			if($_REQUEST[$field] != ''){
				  update_option($field, $_REQUEST[$field]);
				
				  if (isset($_REQUEST[$field])) {
				    $value = preg_replace('/\\\"(.*)\\\"/', "“$1”", $_REQUEST[$field]);
				    update_option($field, $value);
				  } else {
				    delete_option($field);
				  }
			}else{
				echo "<script>alert('Por favor preencha todos os campos de tradução para salvar')</script>";
			}
		}

	// Adiciona a página no admin
		function main_admin_add() {
		    add_theme_page("Custom Design", "Custom Design", 'edit_theme_options', basename(__FILE__), 'main_admin_html', '', 3);
		}

	// Página que sera mostrada na tela do admin
		function main_admin_html() {
		    // Form control
		    $form_sent = false;
		
		    if (isset($_REQUEST['save_options'])) {
		        
		        /*Setando os valores nos campos*/
		        
			        /*Informações Gerais*/
			        	up_del_new_field('name_unidade');
			        	up_del_new_field('unidade_tel_1');
			        	up_del_new_field('unidade_tel_2');
			        	up_del_new_field('unidade_tel_3');
			        	up_del_new_field('unidade_tel_4');
			        	up_del_new_field('unidade_tel_5');
			        	up_del_new_field('unidade_email');
			        	up_del_new_field('unidade_endereco');
			        
			        /*Redes Sociais*/
			        	up_del_new_field('facebook_unidade');
			        	up_del_new_field('instagram_unidade');
			        	up_del_new_field('twitter_unidade');
			        	up_del_new_field('linkedin_unidade');
			        	up_del_new_field('google_plus_unidade');
			        	up_del_new_field('youtube_unidade');
			        
			        /*Labels Block*/
			        	up_del_new_field('vantages_home');
			        	up_del_new_field('servico_home');
			        	up_del_new_field('form_title_orcamento_home');
			        	up_del_new_field('form_content_orcamento_home');
			        	up_del_new_field('depoimentos_home');
			        	up_del_new_field('unidades_home');
			        	up_del_new_field('blog_home');
			        	up_del_new_field('servico_servico');
			        	up_del_new_field('franquias_unidades');
			        	up_del_new_field('seja_unidade');
			        	up_del_new_field('seja_content_unidade');
			        	up_del_new_field('post_relacionados_unidade');
			        	up_del_new_field('compartilhar_single');
			        	up_del_new_field('publicado_single');
			        	up_del_new_field('copyright_footer');
			        	up_del_new_field('tel_footer');
			        	up_del_new_field('sociais_footer');
			        	up_del_new_field('valores_quem_somos');
			        	
			        	
			        	
			        	
		        		$form_sent = true;
		    }
		
				/*Recuperando os valores dos campos*/
					
					/*Informações Gerais*/
				    	$name_unidade      = trim(addslashes(strip_tags(get_option('name_unidade'))));
				    	
				    	$unidade_tel_1      = trim(addslashes(strip_tags(get_option('unidade_tel_1'))));
				    	$unidade_tel_2      = trim(addslashes(strip_tags(get_option('unidade_tel_2'))));
				    	$unidade_tel_3      = trim(addslashes(strip_tags(get_option('unidade_tel_3'))));
				    	$unidade_tel_4      = trim(addslashes(strip_tags(get_option('unidade_tel_4'))));
				    	$unidade_tel_5      = trim(addslashes(strip_tags(get_option('unidade_tel_5'))));
				    	$unidade_email      = trim(addslashes(strip_tags(get_option('unidade_email'))));
				    	
				    
				    /*/*Redes Sociais*/	
				    	$facebook_unidade  = trim(addslashes(strip_tags(get_option('facebook_unidade'))));
				    	$instagram_unidade = trim(addslashes(strip_tags(get_option('instagram_unidade'))));
				    	$twitter_unidade   = trim(addslashes(strip_tags(get_option('twitter_unidade'))));
				    	$linkedin_unidade  = trim(addslashes(strip_tags(get_option('linkedin_unidade'))));
				    	$google_plus_unidade  = trim(addslashes(strip_tags(get_option('google_plus_unidade'))));
				    	$youtube_unidade  = trim(addslashes(strip_tags(get_option('youtube_unidade'))));
				    	$unidade_endereco  = trim((get_option('unidade_endereco')));
				    	
				    /*Labels Block */
				    	$vantages_home = trim((get_option('vantages_home')));
				    	$servico_home = trim((get_option('servico_home')));
				    	$form_title_orcamento_home = trim((get_option('form_title_orcamento_home')));
				    	$form_content_orcamento_home = trim((get_option('form_content_orcamento_home')));
						$depoimentos_home = trim((get_option('depoimentos_home')));
						$unidades_home = trim((get_option('unidades_home')));
						$blog_home = trim((get_option('blog_home')));
						$servico_servico = trim((get_option('servico_servico')));
						$franquias_unidades = trim((get_option('franquias_unidades')));
						$seja_unidade = trim((get_option('seja_unidade')));
						$seja_content_unidade = trim((get_option('seja_content_unidade')));
						$post_relacionados_unidade = trim((get_option('post_relacionados_unidade')));
						$compartilhar_single = trim((get_option('compartilhar_single')));
						$publicado_single = trim((get_option('publicado_single')));
						$copyright_footer = trim((get_option('copyright_footer')));
						$tel_footer = trim((get_option('tel_footer')));
						$sociais_footer = trim((get_option('sociais_footer')));
						$valores_quem_somos = trim((get_option('valores_quem_somos')));
						
									    	
		    ?>
			
			<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
			
			<style type="text/css">
				
				input[type='text'].form-control {
					width: 100%;
				}
				
				
			</style>
				
			<script>
			  jQuery(function() {
			   	jQuery( "#accordion" ).accordion({ collapsible: true});
			   	
			   	jQuery('#form_set').submit(function(){
			   		if($('#form_set .form-control').val() == ''){
			   			alert('Por favor preencha todos os campos');
			   			return false;
			   		}
			   	});
			   	
			  });
			</script>
				
			
			<form class="admin-wrap admin-main" id="form_set" class="form-control"  required="required" name="adm-main-form" method="post" action="">
				
				<div class="container">
					
				<h3 class="title_config"> Configurações  do tema </h3>
					
						
				</div>
				
				<div id="accordion">

					<h3>Informações Gerais</h3>
					<div>
					   	  <p>Nome da unidade</p>
					   	  <input type="text" class="form-control"  required="required" name="name_unidade" value="<?php echo $name_unidade; ?>" /><br />
					   	  
					   	  <p>Telefone 1</p>
					   	  <input type="text" class="form-control"  required="required" name="unidade_tel_1" value="<?php echo $unidade_tel_1; ?>" /><br />
					   	  
					   	  <p>Telefone 2</p>
					   	  <input type="text" class="form-control"  required="required" name="unidade_tel_2" value="<?php echo $unidade_tel_2; ?>" /><br />
					   	  
					   	  <p>Telefone 3</p>
					   	  <input type="text" class="form-control"  required="required" name="unidade_tel_3" value="<?php echo $unidade_tel_3; ?>" /><br />
					   	  
					   	  <p>Telefone 4</p>
					   	  <input type="text" class="form-control"  required="required" name="unidade_tel_4" value="<?php echo $unidade_tel_4; ?>" /><br />
					   	  
					   	  <p>Telefone 5</p>
					   	  <input type="text" class="form-control"  required="required" name="unidade_tel_5" value="<?php echo $unidade_tel_5; ?>" /><br />
					   	  
					   	  <p>E-mail</p>
					   	  <input type="text" class="form-control"  required="required" name="unidade_email" value="<?php echo $unidade_email; ?>" /><br />
					   	  
					   	  <p>Endereço</p>
					   	  <input type="text" class="form-control"  required="required" name="unidade_endereco" value="<?php echo $unidade_endereco; ?>" /><br />
					   	  
					</div>

		        
		        
			        <h3>Redes Sociais</h3>
			        <div>
					   	  <p>Facebook</p>
					   	  <input type="text" class="form-control"  required="required" name="facebook_unidade" value="<?php echo $facebook_unidade; ?>" /><br />
					   	  
					   	  <p>Instagram</p>
					   	  <input type="text" class="form-control"  required="required" name="instagram_unidade" value="<?php echo $instagram_unidade; ?>" /><br />
					   	  
					   	  <p>Twitter</p>
					   	  <input type="text" class="form-control"  required="required" name="twitter_unidade" value="<?php echo $twitter_unidade; ?>" /><br />
					   	  
					   	  <p>Linkedin</p>
					   	  <input type="text" class="form-control"  required="required" name="linkedin_unidade" value="<?php echo $linkedin_unidade; ?>" /><br />
					   	  
					   	  <p>Google Plus</p>
					   	  <input type="text" class="form-control"  required="required" name="google_plus_unidade" value="<?php echo $google_plus_unidade; ?>" /><br />
					   	  
					   	  <p>YouTube</p>
					   	  <input type="text" class="form-control"  required="required" name="youtube_unidade" value="<?php echo $youtube_unidade; ?>" /><br />
					</div>
					
					<h3>Labels</h3>
					<div>
						
						<p>Label - <?php echo substr(strip_tags($vantages_home), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="vantages_home" value="<?php echo $vantages_home ?>" />
						
						<p>Label - <?php echo substr(strip_tags($servico_home), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="servico_home" value="<?php echo $servico_home ?>" />
						
						<p>Label - <?php echo substr(strip_tags($form_title_orcamento_home), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="form_title_orcamento_home" value="<?php echo $form_title_orcamento_home ?>" />
						
						<p>Label - <?php echo substr(strip_tags($form_content_orcamento_home), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="form_content_orcamento_home" value="<?php echo $form_content_orcamento_home ?>" />
						
						<p>Label - <?php echo substr(strip_tags($form_content_orcamento_home), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="form_content_orcamento_home" value="<?php echo $form_content_orcamento_home ?>" />

						<p>Label - <?php echo substr(strip_tags($depoimentos_home), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="depoimentos_home" value="<?php echo $depoimentos_home ?>" />
						
						<p>Label - <?php echo substr(strip_tags($unidades_home), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="unidades_home" value="<?php echo $unidades_home ?>" />
						
						<p>Label - <?php echo substr(strip_tags($blog_home), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="blog_home" value="<?php echo $blog_home ?>" />
						
						<p>Label - <?php echo substr(strip_tags($servico_servico), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="servico_servico" value="<?php echo $servico_servico ?>" />
						
						<p>Label - <?php echo substr(strip_tags($franquias_unidades), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="franquias_unidades" value="<?php echo $franquias_unidades ?>" />
						
						<p>Label - <?php echo substr(strip_tags($seja_unidade), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="seja_unidade" value="<?php echo $seja_unidade ?>" />
						
						<p>Label - <?php echo substr(strip_tags($seja_content_unidade), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="seja_content_unidade" value="<?php echo $seja_content_unidade ?>" />
						
						<p>Label - <?php echo substr(strip_tags($post_relacionados_unidade), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="post_relacionados_unidade" value="<?php echo $post_relacionados_unidade ?>" />
						
						<p>Label - <?php echo substr(strip_tags($compartilhar_single), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="compartilhar_single" value="<?php echo $compartilhar_single ?>" />
						
						<p>Label - <?php echo substr(strip_tags($publicado_single), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="publicado_single" value="<?php echo $publicado_single ?>" />
						
						<p>Label - <?php echo substr(strip_tags($copyright_footer), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="copyright_footer" value="<?php echo $copyright_footer ?>" />
						
						<p>Label - <?php echo substr(strip_tags($tel_footer), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="tel_footer" value="<?php echo $tel_footer ?>" />
						
						<p>Label - <?php echo substr(strip_tags($sociais_footer), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="sociais_footer" value="<?php echo $sociais_footer ?>" />
						
						<p>Label - <?php echo substr(strip_tags($valores_quem_somos), 0,250) ?></p>
						<input type="text" class="form-control"  required="required" name="valores_quem_somos" value="<?php echo $valores_quem_somos ?>" />
						
						
						
						
						
						
						
						
					</div>
					
				</div>
			   <input type="submit" class="form-control"  required="required" name="save_options" value="Salvar" />
		    
		    </form> <!-- .admin-wrap -->
		    
		    <?php } add_action('admin_menu' , 'main_admin_add');
		
		
