<?php

function filterSearch($query) {
    if ($query->is_search) {
        $query->set('post_type',array('post'));
    }
return $query;
}
add_filter('pre_get_posts','filterSearch');


	/*CUSTOM POST SERVIÇOS*/
		add_action( 'init', 'create_post_type_servicos' );
		function create_post_type_servicos() {
				register_post_type( 'servicos',
					array(
					'labels' => array(
						'name' => __( 'Serviços' ),
						'singular_name' => __( 'servico' ),
						'all_items' => __('Todas os serviços'),
						'add_new' => __('Novo serviço'),
						'add_new_item' => __('Adicionar serviço'),
					),
					
					'taxonomies'         => array('categorias-servicos'), /*GET TAXONOMY - COMENTAR ESSA LINHA SE NÃO UTILIZAR TAXONOMIA*/
					'public'             => true,
					'publicly_queryable' => true,
					'show_ui'            => true,
					'show_in_menu'       => true,
					'query_var'          => true,
					'rewrite'            => array( 'slug' => 'servicos' ),
					'capability_type'    => 'post',
					'has_archive'        => true,
					'hierarchical'       => true,
					'menu_position'      => 5,
					'menu_icon'   => 'dashicons-hammer',
					/* TROCAR ÍCONE https://developer.wordpress.org/resource/dashicons/ */
					'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
					/* array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields', ) */
					)
				);
		}
	
	/*CUSTOM POST DEPOIMENTOS*/	
		add_action( 'init', 'create_post_type_depoimentos' );
		function create_post_type_depoimentos() {
				register_post_type( 'depoimentos',
					array(
					'labels' => array(
						'name' => __( 'Depoimentos' ),
						'singular_name' => __( 'Depoimento' ),
						'all_items' => __('Todas os depoimentos'),
						'add_new' => __('Novo depoimento'),
						'add_new_item' => __('Adicionar depoimento'),
					),
								//'taxonomies'         => array('categoria-exemplo'), /*GET TAXONOMY - COMENTAR ESSA LINHA SE NÃO UTILIZAR TAXONOMIA*/
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'depoimentos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => true,
								'menu_position'      => 5,
								'menu_icon'   => 'dashicons-list-view',
								/* TROCAR ÍCONE https://developer.wordpress.org/resource/dashicons/ */
								'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
								/* array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields', ) */
						)
				);
		}
	
	/*CUSTOM POST VANTAGENS*/	
		add_action( 'init', 'create_post_type_vantagens' );
		function create_post_type_vantagens() {
				register_post_type( 'vantagens',
					array(
					'labels' => array(
						'name' => __( 'Vantagens' ),
						'singular_name' => __( 'Vantagem' ),
						'all_items' => __('Todas as vantagens'),
						'add_new' => __('Nova vantagem'),
						'add_new_item' => __('Adicionar vantagem'),
					),
								'taxonomies'         => array('categorias-vantagens'), /*GET TAXONOMY - COMENTAR ESSA LINHA SE NÃO UTILIZAR TAXONOMIA*/
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'vantagens' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => true,
								'menu_position'      => 5,
								'menu_icon'   => 'dashicons-chart-bar',
								/* TROCAR ÍCONE https://developer.wordpress.org/resource/dashicons/ */
								'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
								/* array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields', ) */
						)
				);
		}

	/*CUSTOM POST UNIDADES*/	
		add_action( 'init', 'create_post_type_unidades' );
		function create_post_type_unidades() {
				register_post_type( 'unidades',
					array(
					'labels' => array(
						'name' => __( 'Unidades' ),
						'singular_name' => __( 'Unidade' ),
						'all_items' => __('Todas as unidades'),
						'add_new' => __('Nova unidade'),
						'add_new_item' => __('Adicionar unidade'),
					),
								'taxonomies'         => array('categorias-unidades'), /*GET TAXONOMY - COMENTAR ESSA LINHA SE NÃO UTILIZAR TAXONOMIA*/
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'unidades' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => true,
								'menu_position'      => 5,
								'menu_icon'   => 'dashicons-chart-bar',
								/* TROCAR ÍCONE https://developer.wordpress.org/resource/dashicons/ */
								'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
								/* array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields', ) */
						)
				);
		}


	/*CUSTOM POST BEM VINDO*/	
		add_action( 'init', 'create_post_type_bem_vindo' );
		function create_post_type_bem_vindo() {
				register_post_type( 'bem_vindo',
					array(
					'labels' => array(
						'name' => __( 'Bem vindo' ),
						'singular_name' => __( 'Posts' ),
						'all_items' => __('Todas os posts'),
						'add_new' => __('Novo post'),
						'add_new_item' => __('Adicionar post'),
					),
								//'taxonomies'         => array('categorias-unidades'), /*GET TAXONOMY - COMENTAR ESSA LINHA SE NÃO UTILIZAR TAXONOMIA*/
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'bem-vindo-home' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => true,
								'menu_position'      => 5,
								'menu_icon'   => 'dashicons-chart-bar',
								/* TROCAR ÍCONE https://developer.wordpress.org/resource/dashicons/ */
								'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
								/* array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields', ) */
						)
				);
		}
		
	/*CUSTOM POST BEM VINDO*/	
		add_action( 'init', 'create_post_type_midia' );
		function create_post_type_midia() {
				register_post_type( 'midia',
					array(
					'labels' => array(
						'name' => __( 'Na mídia' ),
						'singular_name' => __( 'Posts' ),
						'all_items' => __('Todas os posts'),
						'add_new' => __('Novo post'),
						'add_new_item' => __('Adicionar post'),
					),
								'taxonomies'         => array('categorias-midia'), /*GET TAXONOMY - COMENTAR ESSA LINHA SE NÃO UTILIZAR TAXONOMIA*/
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'midia' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => true,
								'menu_position'      => 5,
								'menu_icon'   => 'dashicons-chart-bar',
								/* TROCAR ÍCONE https://developer.wordpress.org/resource/dashicons/ */
								'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments')
								/* array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields', ) */
						)
				);
		}

		
