<?php get_header() ?>
	<!--PART TEXTO QUEM SOMOS-->
		<div class="container-fluid content-1-quem-somos">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="container">
							<div class="col-md-12">
								<?php while(have_posts()){ the_post() ?>
										<article>
											<h4 class="page-title-2-home page-title3 title-qum-somos"><?php the_title(); ?></h4>
											<p class="page-subtitle subtitle-quem-somos"></p>
											<?php the_content(); ?>
										</article>
								<?php } ?>	
							</div>
						</div>	
					</div>	
				</div>	
			</div>
		</div>
	
	<!--PART DEPOIMENTOS-->
		<div class="part-depoimentos-quem-somos">
			<?php get_template_part('partials/part-depoimentos') ?>
		</div>

	<!--PART BLOG-->
		<div class="part-blog-quem-somos">
			<?php get_template_part('partials/part-blog') ?>
		</div>
	
<?php get_footer() ?>