<?php get_header() ?>

<?php
		$catg = null;

		/*ARGUMENTOS PARA RETORNAR AS CATEGORIAS(ESTADOS) DAS UNIDADES*/
		$args = array(
		 'type'                     => 'seja-um-franqueado',
		 'child_of'                 => 0,
		 'parent'                   => 0,
 		 'orderby'                  => '',
		 'order'                    => 'Asc',
		 'hide_empty'               => 0,
		 'hierarchical'             => 1,
		 'exclude'                  => '12',
		 'include'                  => '',
		 'number'                   => '',
		 'taxonomy'                 => 'categorias-unidades',
		 'pad_counts'               => false

		);



	/*PEGANDO AS CATEGORIAS FILTRADAS DE ACORDO COM OS ARGUMENTOS ACIMA*/
		$categories = get_categories($args);



		$arr_category = array();
		$duplicate_values = array();


			foreach($categories as $category)
			{
				
				$index = get_field('order_estado', $category->taxonomy . '_' . $category->term_id);

				$arr_category[$index] = $category;
			}


			ksort($arr_category);
			$categories = $arr_category;



?>
		<!--
		<div class="container-fluid content-1-franquias">
			<div class="row">
				<div class="container">
					<div class="row">

						<?php
							$qry = new WP_Query( array(
						  					"post_type" 	=> 'unidades',
						  					'tax_query' => array(
												array(
													'taxonomy' => 'categorias-unidades',
													'field'    => 'slug',
													'terms'    =>  "cms-conteudo"
												),)));
						?>

							<?php while($qry->have_posts()){ $qry->the_post() ?>


												<div class="col-md-12">
													<h2 class="title-serv title-page title-bem-vindo"><?php the_title() ?></h2>
												</div>

												<div class="col-md-12 conteudo-bem-vindo">
													<?php the_content() ?>
												</div>


							<?php } ?>
					</div>
				</div>
			</div>
		</div>
		-->

		<div class="container-fluid content-1-quem-somos content-header-tabs-franquias" style="min-height: auto; margin-top:-5px;    border-bottom: 50px solid #fff;">
			<div class="row container-unidade-limpeza-zelo" style="background-color: rgba(107, 199, 193, 0.5);padding-bottom: 65px">
				<div class="container">
					<div class="row">

						<div class="col-md-12">
							<div class="col-md-4">
								<img class="bottom-img" src="<?php bloginfo('template_directory')?>/img/logo-redondo-zelo.png"/>
							</div>
							<div class="col-md-8">
								<h1 class="title-serv title-page title-franquias"><?php echo get_option('franquias_unidades') ?></h1>
<!--								<p class="page-subtitle sub-title-serv sub-title-franquias"></p>-->
							</div>
						</div>

						<div class="col-md-12 row">

							<div class="col-md-6 col-sm-6 col-xs-12 container-unidades-left">
							<?php
								$x = 1;
								$rand = 60;

								$middle = ceil( count($categories)/2 );

								foreach($categories as $category){ ?>
								  <?php if($x <= $middle ) { ?>
									<?php $cat1[] = $category->cat_ID;  ?>
									<div class="container-cat">
										<h4 class="cat_name" style="">
											<?php
												echo  $category->name;
											?>
										</h4>


											  <ul>
											    <div class="container-li">
												<?php
													$query = new WP_Query( array(
															  					"post_type" 	=> 'unidades',
																				'showposts' => 100,
															  					"orderby"			=> 'name',
															  					"order" => 'ASC',
															  					'tax_query' => array(
																					array(
																						'taxonomy' => 'categorias-unidades',
																						'field'    => 'slug',
																						'terms'    =>  $category->slug,
																					),)));
												?>

												<?php 	while($query->have_posts()) { $query->the_post(); ?>

															<li>
																<a href="<?php echo   get_protocol(); the_field('linkunidade'); ?>" title="<?php echo get_the_title()  ?>">
																	<?php the_title() ?>
																</a>
															</li>
												<?php  }  ?>
												</div>
											</ul>

										</div>
										<?php $x++; } } ?>



						</div>






							<?php

								/*ARGUMENTOS PARA RETORNAR AS CATEGORIAS(ESTADOS) DAS UNIDADES*/
									$cat1[] = 19 ;

									$args2 = array(
										 'type'                     => 'seja-um-franqueado',
										 'child_of'                 => 0,
										 'parent'                   => 0,
										 'orderby'                  => '',
										 'order'                    => 'Asc',
										 'hide_empty'               => 0,
										 'hierarchical'             => 1,
										 'exclude'                  => implode(',', $cat1),
										 'include'                  => '',
										 'number'                   => '',
										 'taxonomy'                 => 'categorias-unidades',
										 'pad_counts'               => false

										);

								/*PEGANDO AS CATEGORIAS FILTRADAS DE ACORDO COM OS ARGUMENTOS ACIMA*/

									// $categories2 = get_categories($args2);
								$last_index = $x - 1;




								$categories2 = array_slice($categories,$middle);





							?>

							<div class="col-md-6 col-sm-6 col-xs-12 container-unidades-right">

								<?php foreach($categories2 as $category){ $rand = 60; ?>

													<div class="container-cat">

														<ul>

															<h4 class="cat_name" style="text-align: right;">
																 <?php echo $category->name; ?>
															</h4>



															 <ul>
															  <div class="container-li">
																<?php
																	$query = new WP_Query( array(
																			  					"post_type" 	=> 'unidades', 'showposts' => 100,
																			  					"orderby"			=> 'name',
																			  					"order" => 'ASC',
																			  					'tax_query' => array(
																									array(
																										'taxonomy' => 'categorias-unidades',
																										'field'    => 'slug',
																										'terms'    =>  $category->slug,
																									),)));
																?>

																<?php 	while($query->have_posts()) { $query->the_post(); ?>

																			<li>
																				<a href="<?php echo   get_protocol(); the_field('linkunidade'); ?>" title="<?php echo get_the_title()  ?>" >
																					<?php the_title() ?>
																				</a>
																			</li>
																<?php  } ?>
																</div>
															</ul>


													</div>

								<?php } ?>

							</div>




						</div>


					</div>
				</div>
			</div>
		</div>

  		<!-- ##################################################################
  		#																	  #
  		#    			      UNIDADES LIMPEZA POR HORA                       #
  		#                                                                     #
  		################################################################### -->


	<div class="container-fluid content-1-limpeza-hora content-header-tabs-franquias" style="min-height: 940px; padding-bottom: 100px; padding-top:40px;">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<img class="bottom-img" src="<?php bloginfo('template_directory')?>/img/logo-redondo-hora.png"/>
						</div>
						<div class="col-md-8">
							<h1 class="title-serv title-page title-franquias">UNIDADES LIMPEZA POR HORA</h1>
<!--							<p class="page-subtitle sub-title-serv sub-title-franquias"></p>-->
						</div>
					</div>
					<div class="col-md-7 col-md-offset-3">
						<div class="container-cat">
							<h4 class="cat_name" style="text-align: center; font-weight: 600">
								Cidade de São Paulo
							</h4>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-3 col-sm-6 col-xs-12 limpeza-hora-box unidades-limpezaporhora-title">
							<div class="container-cat">
								<h3>Zona Sul</h3>

								<ul class="unidades-list">

										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">JABAQUARA</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">JARDIM SAÚDE</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">SAÚDE</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">SANTA CRUZ</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">CHACARA KLABIN</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">SACOMÃ</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA MASCOTE</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">BROOKLIN NOVO E VELHO</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA CORDEIRO</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA CRUZEIRO</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">CHÁCARA SANTO ANTÔNIO</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA OLÍMPIA</a></li>
										<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">INDIANÓPOLIS</a></li>


								</ul>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 limpeza-hora-box unidades-limpezaporhora-title">
							<div class="container-cat">
								<h3>Zona Oeste</h3>
								<ul class="unidades-list">

									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">PINHEIROS</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA LEOPODINA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">LAPA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">JARDIM EUROPA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA MADALENA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">ALTO DE PINHEIROS</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">SUMARÉ</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA ROMANA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">PIRITUBA</a></li>



								</ul>
							</div>

						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 limpeza-hora-box unidades-limpezaporhora-title">
							<div class="container-cat">
								<h3>Zona Norte</h3>
								<ul class="unidades-list">


									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">SANTANA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA GUILHERME</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">CASA VERDE</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">LIMÃO</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA MARIA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA PAIVA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">SANTA TEREZINHA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">JARDIM ANDARAÍ</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">IMIRIM</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">MANDAQUI</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">TUCURUVI</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA NOVA MAZZEI</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">TREMEMBÉ</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">LAUZANE PAULISTA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">JARDIM BRASIL</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA MEDEIROS</a></li>



								</ul>
							</div>

						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 limpeza-hora-box unidades-limpezaporhora-title">
							<div class="container-cat">
								<h3>Zona Leste</h3>
								<ul class="unidades-list">




									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">ARTHUR ALVIM</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">TATUAPÉ</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">PENHA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">CIDADE PATRIARCA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA MATILDE</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA RÉ</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">GUAIANAZES</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA CARMOSINA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">MOOCA</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">VILA CARRÃO</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">BELÉM</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">BRÁS</a></li>
									<li><a href="http://limpezaporhora-contratar.limpezacomzelo.com.br/#/passo-1">ARICANDUVA</a></li>


								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--PART DEPOIMENTOS-->
		<div class="part-depoimentos-quem-somos">
			<?php get_template_part('partials/part-depoimentos') ?>
		</div>

	<!--PART BLOG-->
		<div class="part-blog-quem-somos">
			<?php get_template_part('partials/part-blog') ?>
		</div>

<?php get_footer() ?>