<?php get_header() ?>
<?php
	
	$x = null;
	$y = null;

?>	
		
<div class="container-fluid content-7 container-category-blog">
	<div class="row">
		<div class="container">
				
				<div class="row">
					<div class="col-md-12">
						<h1 class="page-title4 title-blog">
							<?php 
								$category = get_category(get_query_var('cat'));
								echo get_cat_name($category->cat_ID);
							?>
						</h1>
						<p class="page-subtitle sub-title-blog"></p>
					</div>
				</div>
				
				<div class="row container-blog">
								
							
								
						<?php  if(have_posts()){$x=1;while(have_posts()){the_post(); ?>		
						
									<div class="col-md-4 col-sm-4 col-xs-10 category-blog  list-search <?php if($x == 3){ echo "last"; $x=0; }?>">
										<figure>
											<a href="<?php the_permalink() ?>" title="<?php get_the_title() ?>">
												<span class="glyphicon glyphicon-share-alt"></span>
												<?php the_post_thumbnail('thumb_blog_home', array( 'alt' =>  get_the_title(), 'title' => get_the_title() )) ?>
											</a>
											
											<figcaption>
												<a href="<?php the_permalink() ?>" title="<?php echo get_the_title() ?>">
													<h3><?php the_title() ?></h3>
												</a>	
												<?php the_excerpt() ?>
												<a href="<?php the_permalink() ?>" class="more-blog" title="Lêr mais sobre <?php echo get_the_title() ?>">Leia Mais</a>
											</figcaption>
										</figure>
									</div>
							
				  	<?php  $y++;$x++;} }else{ ?> 
				  
				  				<div class="col-md-4 col-sm-4 col-xs-12 cont-blog">
									<span>Nenhum post encontrado</span>
								</div>
				  	<?php } ?>
					
				</div>	
				
				<div class="row">
					<div class="col-md-12 pagination ">
						<?php wp_pagination() ?>
					</div>
				</div>
				
		</div>
	</div>
	
</div>	

					

<?php get_footer() ?>